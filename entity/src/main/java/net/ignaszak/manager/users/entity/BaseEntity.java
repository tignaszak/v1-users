package net.ignaszak.manager.users.entity;

import lombok.Getter;

import javax.persistence.*;
import java.util.UUID;

@Getter
@MappedSuperclass
public abstract class BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private final String publicId = UUID.randomUUID().toString();
}
