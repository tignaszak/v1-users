package net.ignaszak.manager.users.security.config;

import net.ignaszak.manager.commons.conf.properties.ManagerProperties;
import net.ignaszak.manager.users.config.AppConfig;
import net.ignaszak.manager.users.connector.tokens.TokensConnectorInterface;
import net.ignaszak.manager.commons.jwt.JwtOAuth2Service;
import net.ignaszak.manager.commons.jwt.JwtService;
import net.ignaszak.manager.commons.spring.config.AuthorizationFilter;
import net.ignaszak.manager.commons.spring.config.CommonForbiddenEntryPoint;
import net.ignaszak.manager.commons.spring.config.CommonWebMvcConfig;
import net.ignaszak.manager.commons.spring.security.mapper.UserSecurityMapper;
import net.ignaszak.manager.commons.spring.security.matcher.UriMatcher;
import net.ignaszak.manager.users.repository.user.UserRepository;
import net.ignaszak.manager.users.security.mapper.UserPrincipalMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.servlet.Filter;

import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

@EnableWebSecurity
@Import(CommonWebMvcConfig.class)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;
    private final JwtService jwtService;
    private final UserSecurityMapper userSecurityMapper;
    private final UserPrincipalMapper userPrincipalMapper;
    private final UriMatcher uriMatcher;
    private final TokensConnectorInterface tokensConnector;
    private final String pathPrefix;
    private final UserRepository userRepository;
    private final AppConfig appConfig;

    public SecurityConfig(@Qualifier("customUserDetailsService") final UserDetailsService userDetailsService,
                          final JwtService jwtService,
                          final UserSecurityMapper userSecurityMapper,
                          final UserPrincipalMapper userPrincipalMapper,
                          final UriMatcher uriMatcher,
                          final TokensConnectorInterface tokensConnector,
                          final ManagerProperties managerProperties,
                          final UserRepository userRepository,
                          final AppConfig appConfig) {
        this.userDetailsService = userDetailsService;
        this.jwtService = jwtService;
        this.userSecurityMapper = userSecurityMapper;
        this.userPrincipalMapper = userPrincipalMapper;
        this.uriMatcher = uriMatcher;
        this.tokensConnector = tokensConnector;
        this.pathPrefix = managerProperties.getServer().getPathPrefix();
        this.userRepository = userRepository;
        this.appConfig = appConfig;
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        final Filter jwtFilter = JwtAuthenticationFilter.builder()
                .authenticationManager(authenticationManager())
                .jwtService(jwtService)
                .userPrincipalMapper(userPrincipalMapper)
                .tokensConnector(tokensConnector)
                .pathPrefix(pathPrefix)
                .build();

        final Filter authFilter = new AuthorizationFilter(
                authenticationManager(),
                userSecurityMapper,
                jwtService,
                new JwtOAuth2Service() {},
                uriMatcher
        );

        final Filter userStatusFilter = new UserStatusFilter(uriMatcher, userRepository, appConfig);

        http
            .csrf().disable()
            .sessionManagement().sessionCreationPolicy(STATELESS)
            .and()
            .exceptionHandling().authenticationEntryPoint(new CommonForbiddenEntryPoint())
            .and()
            .addFilter(jwtFilter)
            .addFilter(authFilter)
            .addFilterAfter(userStatusFilter, AuthorizationFilter.class);
    }

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth
            .userDetailsService(userDetailsService)
            .passwordEncoder(bCryptPasswordEncoder());
    }

    @Bean
    public PasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
