package net.ignaszak.manager.users.facade;

import net.ignaszak.manager.commons.rest.page.PageRequest;
import net.ignaszak.manager.commons.rest.response.DataResponse;
import net.ignaszak.manager.commons.rest.response.ListResponse;
import net.ignaszak.manager.users.rest.filter.UserFilterRequest;
import net.ignaszak.manager.users.rest.request.UserRegistrationRequest;
import net.ignaszak.manager.users.rest.request.UserResetPasswordRequest;
import net.ignaszak.manager.users.rest.request.UserUpdateRequest;
import net.ignaszak.manager.users.rest.response.UserResponse;

public interface ServiceFacade {
    DataResponse<UserResponse> registerUser(UserRegistrationRequest userRegistrationRequest);
    void activateUser(String publicId, String activationCode);
    void sendResetPasswordTokenAsync(String email);
    DataResponse<UserResponse> getUserByPublicId(String publicId);
    DataResponse<UserResponse> sendActivationCode(String publicId);
    void resetPassword(String token, UserResetPasswordRequest userResetPasswordRequest);
    DataResponse<UserResponse> updateUser(String publicId, UserUpdateRequest userUpdateRequest);
    ListResponse<UserResponse> getUserList(PageRequest pageRequest, UserFilterRequest filter);
    void deleteInactiveUsersAsync();
}
