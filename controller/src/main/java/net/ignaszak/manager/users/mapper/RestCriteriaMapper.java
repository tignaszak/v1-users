package net.ignaszak.manager.users.mapper;

import net.ignaszak.manager.commons.rest.page.PageRequest;
import net.ignaszak.manager.commons.restcriteria.to.CriteriaTO;

public interface RestCriteriaMapper<R, F> {
    net.ignaszak.manager.commons.spring.restcriteria.mapper.RestCriteriaMapper getCommonRestCriteriaMapper();
    CriteriaTO<F> pageRequestAndFilterRequestToCriteriaTO(PageRequest pageRequest, R filterRequest);
}
