package net.ignaszak.manager.users.to;

public record UserUpdateTO(String publicId, String oldPassword, String newPassword, String newPasswordRetyped) {}
