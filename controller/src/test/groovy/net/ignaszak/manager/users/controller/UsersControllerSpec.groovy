package net.ignaszak.manager.users.controller

import net.ignaszak.manager.commons.error.exception.ValidationException
import net.ignaszak.manager.commons.rest.response.DataResponse
import net.ignaszak.manager.users.ControllerSpecification
import net.ignaszak.manager.users.facade.ServiceFacade
import org.spockframework.spring.SpringBean
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest

import static net.ignaszak.manager.commons.jwt.JwtConstants.TOKEN_HEADER
import static net.ignaszak.manager.commons.test.TestUtils.assertErrorResponse
import static net.ignaszak.manager.commons.test.helper.JwtHelper.anyToken
import static net.ignaszak.manager.commons.test.helper.UserHelper.ANY_PUBLIC_ID
import static net.ignaszak.manager.users.TestHelper.anyUserResponse
import static net.ignaszak.manager.users.TestHelper.expectUserResponse
import static net.ignaszak.manager.users.service.error.UserError.USR_USER_EXISTS
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(UsersController)
class UsersControllerSpec extends ControllerSpecification {

    @SpringBean
    ServiceFacade serviceFacadeMock = Mock()

    def "get current user"() {
        when:
        1 * serviceFacadeMock.getUserByPublicId(ANY_PUBLIC_ID) >> new DataResponse<>(anyUserResponse())
        def request = get("/api/me")
                .header(TOKEN_HEADER, anyToken())

        then:
        expectUserResponse(mvc.perform(request), status().isOk())
    }

    def "return error while getting current user"() {
        when:
        1 * serviceFacadeMock.getUserByPublicId(ANY_PUBLIC_ID) >> {throw new ValidationException(USR_USER_EXISTS)}
        def request = get("/api/me")
                .header(TOKEN_HEADER, anyToken())

        then:
        assertErrorResponse(mvc.perform(request), status().isBadRequest(), USR_USER_EXISTS)
    }

    def "return 'UserResponse' by public id"() {
        when:
        1 * serviceFacadeMock.getUserByPublicId(ANY_PUBLIC_ID) >> new DataResponse<>(anyUserResponse())
        def request = get("/api/{publicId}", ANY_PUBLIC_ID)
                .header(TOKEN_HEADER, anyToken())

        then:
        expectUserResponse(mvc.perform(request), status().isOk())
    }

    def "return error while getting user by public id endpoint"() {
        when:
        1 * serviceFacadeMock.getUserByPublicId(ANY_PUBLIC_ID) >> {throw new ValidationException(USR_USER_EXISTS)}
        def request = get("/api/{publicId}", ANY_PUBLIC_ID)
                .header(TOKEN_HEADER, anyToken())

        then:
        assertErrorResponse(mvc.perform(request), status().isBadRequest(), USR_USER_EXISTS)
    }
}
