package net.ignaszak.manager.users.config;

import java.time.Period;

public interface AppConfig {
    Period getDeleteInactiveUserPeriod();
    String getDeleteInactiveUserCronSchedule();
}
