package net.ignaszak.manager.users.security.config;

import lombok.Builder;
import lombok.extern.log4j.Log4j2;
import net.ignaszak.manager.commons.error.Error;
import net.ignaszak.manager.users.connector.tokens.TokensConnectorInterface;
import net.ignaszak.manager.commons.jwt.JwtService;
import net.ignaszak.manager.commons.rest.response.DataResponse;
import net.ignaszak.manager.commons.rest.response.ErrorResponse;
import net.ignaszak.manager.users.connector.tokens.TokensConnectorException;
import net.ignaszak.manager.users.security.mapper.UserPrincipalMapper;
import net.ignaszak.manager.users.security.model.UserDetailsModel;
import org.springframework.http.*;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.stream.Collectors;

import static net.ignaszak.manager.commons.error.AuthError.AUT_AUTHENTICATION;
import static net.ignaszak.manager.commons.jwt.JwtConstants.TOKEN_HEADER;
import static net.ignaszak.manager.commons.spring.config.WebConfigHelper.setResponse;
import static net.ignaszak.manager.commons.spring.security.SecurityConstants.*;
import static net.ignaszak.manager.users.service.error.UserError.USR_BAD_CREDENTIALS;
import static net.ignaszak.manager.users.service.error.UserError.USR_INACTIVE;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

@Log4j2
@Builder
class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;
    private final JwtService jwtService;
    private final UserPrincipalMapper userPrincipalMapper;
    private final TokensConnectorInterface tokensConnector;
    private final String pathPrefix;

    @SuppressWarnings("squid:S1144")
    private JwtAuthenticationFilter(final AuthenticationManager authenticationManager,
                                    final JwtService jwtService,
                                    final UserPrincipalMapper userPrincipalMapper,
                                    final TokensConnectorInterface tokensConnector,
                                    final String pathPrefix) {
        this.authenticationManager = authenticationManager;
        this.jwtService = jwtService;
        this.userPrincipalMapper = userPrincipalMapper;
        this.tokensConnector = tokensConnector;
        this.pathPrefix = pathPrefix;

        this.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher(pathPrefix + AUTH_LOGIN_URL, HttpMethod.POST.name()));
    }

    @Override
    public Authentication attemptAuthentication(final HttpServletRequest request, final HttpServletResponse response) {
        var username = request.getParameter(AUTH_PARAM_USERNAME);
        var password = request.getParameter(AUTH_PARAM_PASSWORD);
        var authenticationToken = new UsernamePasswordAuthenticationToken(username, password);

        return authenticationManager.authenticate(authenticationToken);
    }

    @Override
    protected void successfulAuthentication(final HttpServletRequest request,
                                            final HttpServletResponse response,
                                            final FilterChain filterChain,
                                            final Authentication authentication) {

        final var principal = authentication.getPrincipal();

        if (principal instanceof UserDetailsModel userDetailsModel) {
            final var roles = userDetailsModel.getAuthorities()
                    .stream()
                    .map(GrantedAuthority::getAuthority)
                    .collect(Collectors.toSet());
            final var userPrincipalModel = userDetailsModel.userPrincipalModel();
            final var jwtUserTO = userPrincipalMapper.userPrincipalModelToJwtUserTO(userPrincipalModel);
            final var token = jwtService.buildToken(jwtUserTO, roles);

            try {
                tokensConnector.register(token, userPrincipalModel.publicId());

                response.addHeader(TOKEN_HEADER, token);

                if (userPrincipalModel.isActive()) {
                    final var userResponse = userPrincipalMapper.userPrincipalModelToUserResponse(userPrincipalModel);

                    setResponse(response, HttpStatus.OK, new DataResponse<>(userResponse));
                } else {
                    sendErrorResponse(response, BAD_REQUEST, USR_INACTIVE);
                }
            } catch (TokensConnectorException e) {
                logger.error("Could not register jwt token for user " + userPrincipalModel.publicId(), e);

                sendErrorResponse(response, BAD_REQUEST, AUT_AUTHENTICATION);
            }
        } else {
            sendErrorResponse(response, BAD_REQUEST, AUT_AUTHENTICATION);
        }
    }

    @Override
    protected void unsuccessfulAuthentication(final HttpServletRequest request,
                                              final HttpServletResponse response,
                                              final AuthenticationException failed) {
        logger.info(USR_BAD_CREDENTIALS.toString(), failed);

        sendErrorResponse(response, UNAUTHORIZED, USR_BAD_CREDENTIALS);
    }

    private void sendErrorResponse(final HttpServletResponse response, final HttpStatus httpStatus, final Error error) {
        final var errorResponse = ErrorResponse.Companion.of(
                error.getCode(),
                error.getMessage()
        );

        setResponse(response, httpStatus, errorResponse);
    }
}
