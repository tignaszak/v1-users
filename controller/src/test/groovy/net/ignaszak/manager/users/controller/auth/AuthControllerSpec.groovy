package net.ignaszak.manager.users.controller.auth

import net.ignaszak.manager.commons.rest.response.DataResponse
import net.ignaszak.manager.commons.spring.handler.repository.RequestMappingRepositorySingleton
import net.ignaszak.manager.users.ControllerSpecification
import net.ignaszak.manager.users.facade.ServiceFacade
import net.ignaszak.manager.users.security.model.UserDetailsModel
import net.ignaszak.manager.users.security.model.UserPrincipalModel
import org.spockframework.spring.SpringBean
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

import java.nio.charset.StandardCharsets

import static net.ignaszak.manager.commons.jwt.JwtConstants.TOKEN_HEADER
import static net.ignaszak.manager.commons.test.TestUtils.assertErrorResponse
import static net.ignaszak.manager.commons.test.TestUtils.assertNoContentResponse
import static net.ignaszak.manager.commons.test.helper.JwtHelper.anyToken
import static net.ignaszak.manager.commons.test.helper.UserHelper.USER_ROLE
import static net.ignaszak.manager.commons.utils.JsonUtils.toJson
import static net.ignaszak.manager.users.TestHelper.ACTIVATION_CODE
import static net.ignaszak.manager.users.TestHelper.RESET_PASSWORD_TOKEN
import static net.ignaszak.manager.users.TestHelper.anyUserActivationRequest
import static net.ignaszak.manager.users.TestHelper.anyUserRegistrationRequest
import static net.ignaszak.manager.users.TestHelper.anyUserResetPasswordRequest
import static net.ignaszak.manager.users.TestHelper.anyUserResetPasswordTokenRequest
import static net.ignaszak.manager.users.TestHelper.anyUserResponse
import static net.ignaszak.manager.users.TestHelper.anyUserTO
import static net.ignaszak.manager.users.TestHelper.expectUserResponse
import static net.ignaszak.manager.users.service.error.UserError.USR_INACTIVE
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(AuthController)
class AuthControllerSpec extends ControllerSpecification {

    private static final String ANY_PASSWORD = "any password"

    @SpringBean
    private ServiceFacade serviceFacadeMock = Mock()

    def setupSpec() {
        RequestMappingRepositorySingleton.INSTANCE.clear()
    }

    def cleanupSpec() {
        RequestMappingRepositorySingleton.INSTANCE.clear()
    }

    def "successful registration"() {
        given:
        1 * serviceFacadeMock.registerUser(anyUserRegistrationRequest()) >> new DataResponse<>(anyUserResponse())
        def request = post("/api/auth/register")
                .content(toJson(anyUserRegistrationRequest()))
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(StandardCharsets.UTF_8.toString())

        when:
        def response = mvc.perform(request)

        then:
        assertNoContentResponse(response, status().isCreated())
    }

    def "return 'UserResponse' with token after successful login"() {
        given:
        1 * userDetailsService.loadUserByUsername(anyUserTO().email()) >> anyUserDetailsModel()
        1 * jwtServiceMock.buildToken(_, _) >> anyToken()
        1 * securityMapperMock.userPrincipalModelToUserResponse(anyUserPrincipalModel()) >> anyUserResponse()
        def request = post("/api/auth/login")
                .param("username", anyUserTO().email())
                .param("password", ANY_PASSWORD)

        when:
        def response = mvc.perform(request)

        then:
        response.andExpect(header().string(TOKEN_HEADER, anyToken()))
        expectUserResponse(response, status().isOk())
    }

    def "return error with jwt token while logging when user is not activated" () {
        given:
        1 * userDetailsService.loadUserByUsername(anyUserTO().email()) >> anyUserDetailsModel(false)
        1 * jwtServiceMock.buildToken(_, _) >> anyToken()
        def request = post("/api/auth/login")
                .param("username", anyUserTO().email())
                .param("password", ANY_PASSWORD)

        when:
        def response = mvc.perform(request)

        then:
        response.andExpect(header().string(TOKEN_HEADER, anyToken()))
        assertErrorResponse(response, status().isBadRequest(), USR_INACTIVE)
        0 * securityMapperMock.userPrincipalModelToUserResponse(_) >> anyUserResponse()
    }

    def "successful activate user"() {
        given:
        def request = post("/api/auth/activate")
                .content(toJson(anyUserActivationRequest()))
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(StandardCharsets.UTF_8.toString())
                .header(TOKEN_HEADER, anyToken())

        when:
        def response = mvc.perform(request)

        then:
        1 * serviceFacadeMock.activateUser(anyUserTO().publicId(), ACTIVATION_CODE)
        assertNoContentResponse(response)
    }

    def "successful send activate code"() {
        given:
        def request = post("/api/auth/sendActivationCode")
                .header(TOKEN_HEADER, anyToken())

        when:
        def response = mvc.perform(request)

        then:
        1 * serviceFacadeMock.sendActivationCode(anyUserTO().publicId())
        assertNoContentResponse(response)
    }

    def "successful send reset password token"() {
        given:
        def request = post("/api/auth/sendResetPasswordToken")
                .content(toJson(anyUserResetPasswordTokenRequest()))
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(StandardCharsets.UTF_8.toString())

        when:
        def response = mvc.perform(request)

        then:
        assertNoContentResponse(response)
    }

    def "successful reset password"() {
        given:
        def request = post("/api/auth/resetPassword/{token}", RESET_PASSWORD_TOKEN)
                .content(toJson(anyUserResetPasswordRequest()))
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(StandardCharsets.UTF_8.toString())
                .header(TOKEN_HEADER, anyToken())

        when:
        def response = mvc.perform(request)

        then:
        1 * serviceFacadeMock.resetPassword(RESET_PASSWORD_TOKEN, anyUserResetPasswordRequest())
        assertNoContentResponse(response)
    }

    private static UserDetailsModel anyUserDetailsModel(Boolean isActive = true) {
        new UserDetailsModel(anyUserPrincipalModel(isActive), anyUserDetails())
    }

    private static UserPrincipalModel anyUserPrincipalModel(Boolean isActive = true) {
        new UserPrincipalModel(anyUserTO().publicId(), anyUserTO().email(), isActive)
    }

    private static UserDetails anyUserDetails() {
        new UserDetails() {
            @Override
            Collection<? extends GrantedAuthority> getAuthorities() {
                return Set.of(new SimpleGrantedAuthority(USER_ROLE))
            }

            @Override
            String getPassword() {
                return new BCryptPasswordEncoder().encode(ANY_PASSWORD)
            }

            @Override
            String getUsername() {
                return anyUserTO().email()
            }

            @Override
            boolean isAccountNonExpired() {
                return true
            }

            @Override
            boolean isAccountNonLocked() {
                return true
            }

            @Override
            boolean isCredentialsNonExpired() {
                return true
            }

            @Override
            boolean isEnabled() {
                return true
            }
        }
    }
}
