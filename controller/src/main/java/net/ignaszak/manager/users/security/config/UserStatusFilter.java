package net.ignaszak.manager.users.security.config;

import lombok.AllArgsConstructor;
import net.ignaszak.manager.commons.rest.response.ErrorResponse;
import net.ignaszak.manager.commons.spring.security.matcher.UriMatcher;
import net.ignaszak.manager.commons.user.to.UserPrincipalTO;
import net.ignaszak.manager.users.config.AppConfig;
import net.ignaszak.manager.users.repository.user.UserRepository;
import net.ignaszak.manager.users.service.error.UserError;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static net.ignaszak.manager.commons.spring.config.WebConfigHelper.setResponse;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;

@AllArgsConstructor
final class UserStatusFilter implements Filter {

    private final UriMatcher uriMatcher;
    private final UserRepository userRepository;
    private final AppConfig appConfig;

    @Override
    public void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse, final FilterChain filterChain) throws IOException, ServletException {
        if (getContext().getAuthentication() instanceof UsernamePasswordAuthenticationToken token &&
            token.getPrincipal() instanceof UserPrincipalTO userPrincipalTO &&
            servletRequest instanceof HttpServletRequest request &&
            servletResponse instanceof HttpServletResponse response &&
            uriMatcher.match(request.getRequestURI()).isAuthenticated() &&
            userRepository.isUserExpiredAfterPeriod(userPrincipalTO.getPublicId(), appConfig.getDeleteInactiveUserPeriod())) {

            final var errorResponse = ErrorResponse.Companion.of(
                UserError.USR_USER_NOT_FOUND.getCode(),
                UserError.USR_USER_NOT_FOUND.getMessage()
            );

            setResponse(response, HttpStatus.NOT_FOUND, errorResponse);

            return;
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }
}
