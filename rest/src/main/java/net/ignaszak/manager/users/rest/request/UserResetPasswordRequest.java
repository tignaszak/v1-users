package net.ignaszak.manager.users.rest.request;

import io.swagger.v3.oas.annotations.media.Schema;

import static net.ignaszak.manager.users.rest.openapi.ApiConstants.PASSWORD;
import static net.ignaszak.manager.users.rest.openapi.ApiConstants.PASSWORD_REPEATED;

@Schema(title = "User reset password request")
public record UserResetPasswordRequest(
        @Schema(description = PASSWORD, required = true) String password,
        @Schema(description = PASSWORD_REPEATED, required = true) String passwordRepeated
) {}
