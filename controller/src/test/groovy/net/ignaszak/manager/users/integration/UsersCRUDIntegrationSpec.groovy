package net.ignaszak.manager.users.integration

import net.ignaszak.manager.commons.rest.response.DataResponse
import net.ignaszak.manager.commons.rest.response.ListResponse
import net.ignaszak.manager.commons.test.specification.integration.auth.JwtAuthentication
import net.ignaszak.manager.users.integration.builder.UserBuilder
import net.ignaszak.manager.users.rest.request.UserUpdateRequest
import org.springframework.http.ResponseEntity
import org.springframework.util.CollectionUtils

import static net.ignaszak.manager.commons.test.TestUtils.assertOnePageSize
import static net.ignaszak.manager.commons.error.AuthError.AUT_INVALID_TOKEN
import static net.ignaszak.manager.commons.test.helper.UserHelper.ANY_PUBLIC_ID
import static net.ignaszak.manager.users.TestConstants.ANY_ENCRYPTED_PASSWORD
import static net.ignaszak.manager.users.TestConstants.ANY_PASSWORD
import static net.ignaszak.manager.users.service.error.UserError.USR_INVALID_PASSWORD
import static net.ignaszak.manager.users.service.error.UserError.USR_USER_NOT_FOUND
import static org.springframework.http.HttpStatus.BAD_REQUEST
import static org.springframework.http.HttpStatus.FORBIDDEN
import static org.springframework.http.HttpStatus.NOT_FOUND
import static org.springframework.http.HttpStatus.OK

class UsersCRUDIntegrationSpec extends IntegrationSpecification {

    def "get current logged user" () {
        setup:
        def user = userBuilder.newLogged()
        insert(user)

        when:
        def responseEntity = restTemplateAuthFacade.get("/api/me") as ResponseEntity<DataResponse<Map<String, String>>>

        then:
        responseEntity.statusCode == OK
        responseEntity.body.data.publicId == user.publicId
        responseEntity.body.data.email == user.email
    }

    def "return forbidden with unauthenticated request"() {
        when:
        def responseEntity = restTemplateFacade.get("/api/me")

        then:
        assertErrorResponse(responseEntity, FORBIDDEN, AUT_INVALID_TOKEN)
    }

    def "get user by public id" () {
        setup:
        def user = userBuilder.new()
        insert(user)

        when:
        def responseEntity = restTemplateAuthFacade.get("/api/${user.publicId}") as ResponseEntity<DataResponse<Map<String, String>>>

        then:
        responseEntity.statusCode == OK
        responseEntity.body.data.publicId == user.publicId
        responseEntity.body.data.email == user.email
    }

    def "return not found while getting inactive user by public id"() {
        setup:
        def user = userBuilder.new().setIsActive(false)
        insert(user)

        when:
        def responseEntity = restTemplateAuthFacade.get("/api/${user.publicId}") as ResponseEntity<DataResponse>

        then:
        assertErrorResponse(responseEntity, NOT_FOUND, USR_USER_NOT_FOUND)
    }

    def "return not found if user does not exists while getting user by public id" () {
        when:
        def responseEntity = restTemplateAuthFacade.get("/api/${ANY_PUBLIC_ID}")

        then:
        assertErrorResponse(responseEntity, NOT_FOUND, USR_USER_NOT_FOUND)
    }

    @SuppressWarnings("all")
    def "update user by public id" () {
        given:
        def user = userBuilder.newLogged()
        insert(user)
        def newPassword = "newPassword123"
        def request = new UserUpdateRequest(ANY_PASSWORD, newPassword, newPassword)

        when:
        def responseEntity = restTemplateAuthFacade.put("/api/${user.publicId}", request) as ResponseEntity<DataResponse<Map<String, String>>>

        then:
        responseEntity.statusCode == OK
        responseEntity.body.data.publicId == user.publicId
        responseEntity.body.data.email == user.email

        and:
        def stmt = CONNECTION.prepareStatement("SELECT password FROM user_account WHERE public_id = ?")
        stmt.setString(1, user.publicId)
        def resultSet = stmt.executeQuery()
        resultSet.next()
        resultSet.getString("password") != ANY_ENCRYPTED_PASSWORD
    }

    def "return error while updating user with invalid old password" () {
        given:
        def user = userBuilder.newLogged()
        insert(user)
        def request = new UserUpdateRequest("invalidPassword123", "newPassword123", "newPassword123")

        when:
        def responseEntity = restTemplateAuthFacade.put("/api/${user.publicId}", request)

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, USR_INVALID_PASSWORD)

        and:
        assertPasswordHasNotChanged(user.publicId)
    }

    def "return error while updating user with invalid new password" () {
        given:
        def user = userBuilder.newLogged()
        insert(user)
        def request = new UserUpdateRequest(ANY_PASSWORD, "newPassword123", "otherNewPassword123")

        when:
        def responseEntity = restTemplateAuthFacade.put("/api/${user.publicId}", request)

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, USR_INVALID_PASSWORD)

        and:
        assertPasswordHasNotChanged(user.publicId)
    }

    def "return active user list" () {
        setup:
        def user1 = userBuilder.new()
        def user2 = userBuilder.new()
        def user3 = userBuilder.new().setIsActive(false)
        insert(user1)
        insert(user2)
        insert(user3)

        when:
        def responseEntity = restTemplateAuthFacade.get("/api") as ResponseEntity<ListResponse<Map<String, String>>>

        then:
        responseEntity.statusCode == OK
        pagingAssertion(responseEntity)
                .withTotalElements(2)
                .isValid()
        !CollectionUtils.isEmpty(responseEntity.body.list)
        responseEntity.body.list.size() == 2
        responseEntity.body.list[0].email == user1.email || user2.email
        responseEntity.body.list[1].email == user1.email || user2.email
    }

    def "paginate active users"(int page, int elements) {
        setup:
        def publicIds = new ArrayList<String>()
        for (int i = 0; i < 46; i++) {
            insert(userBuilder.new().setIsActive(false))
            def publicId = UUID.randomUUID().toString()
            publicIds.add(publicId)
            insert(
                    userBuilder.new()
                            .setPublicId(publicId)
                            .setEmail(publicId + "@email.pl")
            )
        }

        when:
        def responseEntity = restTemplateAuthFacade.get("/api?page=${page}") as ResponseEntity<ListResponse<Map<String, String>>>

        then:
        responseEntity.statusCode == OK
        pagingAssertion(responseEntity)
                .withNumber(page)
                .withTotalPages(3)
                .withTotalElements(46)
                .isValid()
        !CollectionUtils.isEmpty(responseEntity.body.list)
        responseEntity.body.list.size() == elements
        int i = 0
        for (; i < elements; i++) {
            responseEntity.body.list[i].publicId == publicIds.get( ((page - 1) * 20) + i )
        }
        i == elements

        where:
        page | elements
        1    | 20
        2    | 20
        3    | 6
    }

    @SuppressWarnings("all")
    def "filter users"(String filter, UserFilterPredicate predicate) {
        given:
        def users = List.of(
                userBuilder.newLogged(),
                userBuilder.new().setEmail(UUID.randomUUID().toString() + "@search.pl"),
                userBuilder.new().setEmail(UUID.randomUUID().toString() + "@email.pl")
        )
        users.forEach(IntegrationSpecification::insert)

        when:
        def responseEntity = restTemplateAuthFacade.get("/api?filter=${filter}") as ResponseEntity<ListResponse<Map<String, String>>>

        then:
        predicate.test(responseEntity, users)

        where:
        filter                                         | predicate
        "publicId:${JwtAuthentication.USER_PUBLIC_ID}" | (entity, list) -> assertOnePageSize(entity, 1)
        "email::@search.pl"                            | (entity, list) -> assertOnePageSize(entity, 1)
    }

    @FunctionalInterface
    interface UserFilterPredicate {
        boolean test(ResponseEntity<ListResponse<Map<String, String>>> entity, List<UserBuilder> list)
    }
}
