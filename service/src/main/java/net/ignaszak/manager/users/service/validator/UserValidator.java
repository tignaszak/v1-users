package net.ignaszak.manager.users.service.validator;


import net.ignaszak.manager.users.to.UserRegistrationTO;
import net.ignaszak.manager.users.to.UserResetPasswordTO;
import net.ignaszak.manager.users.to.UserUpdateTO;

public interface UserValidator {

    void validRegistration(UserRegistrationTO userRegistrationTO);

    void validEmail(String email);

    void validPublicId(String publicId);

    void validActivationCode(String activationCode);

    void validResetPassword(UserResetPasswordTO userResetPasswordTO);

    void validUpdate(UserUpdateTO userUpdateTO);
}
