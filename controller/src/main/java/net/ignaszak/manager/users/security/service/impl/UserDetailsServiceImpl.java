package net.ignaszak.manager.users.security.service.impl;

import net.ignaszak.manager.commons.error.exception.ValidationException;
import net.ignaszak.manager.users.security.model.UserDetailsModel;
import net.ignaszak.manager.users.security.service.UserSecurityService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("customUserDetailsService")
class UserDetailsServiceImpl implements UserDetailsService {

    private final UserSecurityService userSecurityService;

    UserDetailsServiceImpl(final UserSecurityService userSecurityService) {
        this.userSecurityService = userSecurityService;
    }

    @Override
    public UserDetails loadUserByUsername(final String username) {
        try {
            final var userSecurityModel = userSecurityService.getUserSecurityModelByEmail(username);
            final var userDetails = User.withUsername(username)
                .password(userSecurityModel.password())
                .roles("USER")
                .build();

            return new UserDetailsModel(userSecurityModel.userPrincipalModel(), userDetails);
        } catch (ValidationException e) {
            throw new UsernameNotFoundException(e.getMessage(), e);
        }
    }
}
