package net.ignaszak.manager.users.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Entity
@Table(name = "user_account", uniqueConstraints = {
        @UniqueConstraint(name = "user_account_email", columnNames = {"email"}),
        @UniqueConstraint(name = "user_account_reset_password_token", columnNames = {"resetPasswordToken"})
})
@NoArgsConstructor
public class UserEntity extends BaseEntity {

    @Column(nullable = false)
    String email;

    @Column(nullable = false)
    @Setter
    String password;

    @Column
    @Setter
    boolean isActive;

    @Column
    @Setter
    String activationCode;

    @Column
    @Setter
    LocalDateTime activationCodeExpiration;

    @Column
    @Setter
    String resetPasswordToken;

    @Column
    @Setter
    LocalDateTime resetPasswordTokenExpiration;

    @Column
    LocalDateTime creationDate;

    public UserEntity(@NonNull final String email, @NonNull final String password, @NonNull final LocalDateTime creationDate) {
        this();
        this.email = email;
        this.password = password;
        this.creationDate = creationDate;
    }
}
