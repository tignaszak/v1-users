package net.ignaszak.manager.users.controller.auth;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import net.ignaszak.manager.commons.openapi.annotations.ApiBadRequestResponse;
import net.ignaszak.manager.commons.openapi.annotations.ApiNoContentResponse;
import net.ignaszak.manager.commons.openapi.annotations.UserJwtSecurityRequirement;
import net.ignaszak.manager.commons.spring.handler.annotation.AllowedMapping;
import net.ignaszak.manager.commons.spring.handler.annotation.AuthenticatedInactiveMapping;
import net.ignaszak.manager.commons.user.annotation.UserPrincipal;
import net.ignaszak.manager.commons.user.to.UserPrincipalTO;
import net.ignaszak.manager.users.facade.ServiceFacade;
import net.ignaszak.manager.users.openapi.ApiResetPasswordTokenParameter;
import net.ignaszak.manager.users.rest.request.UserActivationRequest;
import net.ignaszak.manager.users.rest.request.UserRegistrationRequest;
import net.ignaszak.manager.users.rest.request.UserResetPasswordRequest;
import net.ignaszak.manager.users.rest.request.UserResetPasswordTokenRequest;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@RestController
@AllArgsConstructor
@Tag(name = "User auth", description = "User authorization operations")
class AuthController {

    private final ServiceFacade serviceFacade;

    @PostMapping("/auth/register")
    @ResponseStatus(CREATED)
    @AllowedMapping
    @Operation(
        summary = "Register new user",
        description = "New user must be activated via code send to email. After 24 hours inactive user will be deleted."
    )
    @ApiNoContentResponse
    @ApiBadRequestResponse
    public void register(@RequestBody final UserRegistrationRequest userRegistrationRequest) {
        serviceFacade.registerUser(userRegistrationRequest);
    }

    @PostMapping("/auth/activate")
    @ResponseStatus(NO_CONTENT)
    @AuthenticatedInactiveMapping
    @Operation(summary = "Activate user")
    @ApiNoContentResponse
    @ApiBadRequestResponse
    @UserJwtSecurityRequirement
    public void activate(@RequestBody UserActivationRequest userActivationRequest,
                         @UserPrincipal UserPrincipalTO userPrincipalTO) {
        serviceFacade.activateUser(userPrincipalTO.getPublicId(), userActivationRequest.code());
    }

    @PostMapping("/auth/sendActivationCode")
    @ResponseStatus(NO_CONTENT)
    @Operation(summary = "Send email with activation code")
    @AuthenticatedInactiveMapping
    @ApiNoContentResponse
    @UserJwtSecurityRequirement
    public void sendActivationCode(@UserPrincipal UserPrincipalTO userPrincipalTO) {
        serviceFacade.sendActivationCode(userPrincipalTO.getPublicId());
    }

    @PostMapping("/auth/sendResetPasswordToken")
    @ResponseStatus(NO_CONTENT)
    @Operation(summary = "Send email with reset password token")
    @AllowedMapping
    @ApiNoContentResponse
    @ApiBadRequestResponse
    public void sendResetPasswordToken(@RequestBody UserResetPasswordTokenRequest userResetPasswordTokenRequest) {
        serviceFacade.sendResetPasswordTokenAsync(userResetPasswordTokenRequest.email());
    }

    @PostMapping("/auth/resetPassword/{token}")
    @ResponseStatus(NO_CONTENT)
    @Operation(summary = "Reset password")
    @AllowedMapping
    @ApiNoContentResponse
    @ApiBadRequestResponse
    public void resetPassword(@ApiResetPasswordTokenParameter @PathVariable String token,
                              @RequestBody UserResetPasswordRequest userResetPasswordRequest) {
        serviceFacade.resetPassword(token, userResetPasswordRequest);
    }
}
