package net.ignaszak.manager.users.integration.builder

import java.time.LocalDateTime

import static net.ignaszak.manager.commons.test.specification.integration.auth.JwtAuthentication.USER_EMAIL
import static net.ignaszak.manager.commons.test.specification.integration.auth.JwtAuthentication.USER_PUBLIC_ID

class UserBuilder {

    String publicId = UUID.randomUUID().toString()
    String email = publicId + "@email.pl"
    String password = null
    boolean isActive = true
    String activationCode
    LocalDateTime activationCodeExpiration
    String resetPasswordToken
    LocalDateTime resetPasswordTokenExpiration
    LocalDateTime creationDate = LocalDateTime.now()

    def static "new"(String password) {
        new UserBuilder(password)
    }

    def static "newLogged"(String password) {
        def builder = new UserBuilder(password)
        builder.setPublicId(USER_PUBLIC_ID)
        builder.setEmail(USER_EMAIL)
        builder
    }

    private UserBuilder(String password) {
        this.password = password
    }

    UserBuilder setPublicId(final String publicId) {
        this.publicId = publicId
        this
    }

    UserBuilder setEmail(final String email) {
        this.email = email
        this
    }

    UserBuilder setPassword(final String password) {
        this.password = password
        this
    }

    UserBuilder setIsActive(final boolean isActive) {
        this.isActive = isActive
        this
    }

    UserBuilder setActivationCode(final String activationCode) {
        this.activationCode = activationCode
        this
    }

    UserBuilder setActivationCodeExpiration(final LocalDateTime activationCodeExpiration) {
        this.activationCodeExpiration = activationCodeExpiration
        this
    }

    UserBuilder setResetPasswordToken(final String resetPasswordToken) {
        this.resetPasswordToken = resetPasswordToken
        this
    }

    UserBuilder setResetPasswordTokenExpiration(final LocalDateTime resetPasswordTokenExpiration) {
        this.resetPasswordTokenExpiration = resetPasswordTokenExpiration
        this
    }

    UserBuilder setCreationDate(final LocalDateTime creationDate) {
        this.creationDate = creationDate
        this
    }
}
