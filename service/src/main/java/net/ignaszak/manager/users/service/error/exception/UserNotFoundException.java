package net.ignaszak.manager.users.service.error.exception;

import net.ignaszak.manager.commons.error.exception.ErrorException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import static net.ignaszak.manager.users.service.error.UserError.USR_USER_NOT_FOUND;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserNotFoundException extends ErrorException {

    public UserNotFoundException() {
        super(USR_USER_NOT_FOUND);
    }
}
