package net.ignaszak.manager.users.openapi;

import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;

import static net.ignaszak.manager.users.openapi.Description.USER_FILTER_DESCRIPTION;

@Parameter(
    schema = @Schema(implementation = String.class),
    description = USER_FILTER_DESCRIPTION
)
public @interface ApiUserFilterParameter {}
