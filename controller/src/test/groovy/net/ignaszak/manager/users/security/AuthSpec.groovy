package net.ignaszak.manager.users.security

import net.ignaszak.manager.users.ControllerSpecification
import net.ignaszak.manager.users.service.error.UserError
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.stereotype.Controller

import java.nio.charset.StandardCharsets

import static net.ignaszak.manager.commons.spring.security.SecurityConstants.AUTH_LOGIN_URL
import static net.ignaszak.manager.commons.test.TestUtils.assertErrorResponse
import static net.ignaszak.manager.commons.test.helper.JwtHelper.anyJwtUserTO
import static net.ignaszak.manager.commons.test.helper.JwtHelper.anyToken
import static net.ignaszak.manager.commons.test.helper.UserHelper.ANY_EMAIL
import static net.ignaszak.manager.users.TestConstants.PARAM_PASSWORD
import static net.ignaszak.manager.users.TestConstants.PARAM_USERNAME
import static net.ignaszak.manager.users.TestConstants.ANY_PASSWORD
import static net.ignaszak.manager.users.TestHelper.anyUserResponse
import static net.ignaszak.manager.users.TestHelper.expectUserResponse
import static TestSecurityHelper.anyUserDetails
import static net.ignaszak.manager.users.security.TestSecurityHelper.anyUserPrincipalModel
import static org.hamcrest.text.MatchesPattern.matchesPattern
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(EmptyController)
class AuthSpec extends ControllerSpecification {

    private static final String TOKEN_PATTERN = '^Bearer \\S+$'

    @Controller
    class EmptyController {}

    def "return 'UserResponse' after successful login"() {
        given:
        1 * securityMapperMock.userPrincipalModelToUserResponse(anyUserPrincipalModel()) >> anyUserResponse()
        1 * securityMapperMock.userPrincipalModelToJwtUserTO(anyUserPrincipalModel()) >> anyJwtUserTO()
        1 * userDetailsService.loadUserByUsername(ANY_EMAIL) >> anyUserDetails()
        1 * jwtServiceMock.buildToken(anyJwtUserTO(), _) >> anyToken()

        when:
        def request = post("/api" + AUTH_LOGIN_URL)
                .param(PARAM_USERNAME, ANY_EMAIL)
                .param(PARAM_PASSWORD, ANY_PASSWORD)
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .characterEncoding(StandardCharsets.UTF_8.toString())

        then:
        def resultActions = mvc.perform(request)
                .andExpect(header().string('Authorization', matchesPattern(TOKEN_PATTERN)))

        expectUserResponse(resultActions, status().isOk())
    }

    def "return error after unsuccessful login"() {
        given:
        1 * userDetailsService.loadUserByUsername(ANY_EMAIL) >> anyUserDetails()

        when:
        def request = post("/api" + AUTH_LOGIN_URL)
                .param(PARAM_USERNAME, ANY_EMAIL)
                .param(PARAM_PASSWORD, "bad password")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .characterEncoding(StandardCharsets.UTF_8.toString())

        then:
        def resultActions = mvc.perform(request)
                .andExpect(header().doesNotExist('Authorization'))

        assertErrorResponse(resultActions, status().isUnauthorized(), UserError.USR_BAD_CREDENTIALS)
    }
}
