package net.ignaszak.manager.users

import lombok.experimental.UtilityClass
import net.ignaszak.manager.commons.restcriteria.paging.query.PageQuery
import net.ignaszak.manager.commons.restcriteria.paging.to.PageCriteriaTO
import net.ignaszak.manager.commons.restcriteria.query.CriteriaQuery
import net.ignaszak.manager.commons.restcriteria.to.CriteriaTO
import net.ignaszak.manager.users.entity.UserEntity
import net.ignaszak.manager.users.to.UserFilterTO
import net.ignaszak.manager.users.to.UserRegistrationTO
import net.ignaszak.manager.users.to.UserResetPasswordTO
import net.ignaszak.manager.users.to.UserTO

import java.time.LocalDateTime

@UtilityClass
final class TestHelper {

    public static final String ENCODED_PASSWORD = "encodedPassword123"
    public static final String NEW_ENCODED_PASSWORD = "newEncodedPassword123"

    public static final String INVALID_EMAIL = "invalid@email"
    public static final String INVALID_NO_DIGIT_PASSWORD = "aaaaaaaaa"
    public static final String INVALID_NO_LETTER_PASSWORD = "123456789"
    public static final String INVALID_SHORT_PASSWORD = "aaaa123"

    public static final String VALID_EMAIL = "valid@email.com"
    public static final String VALID_PASSWORD = "validPassword123"
    public static final String NEW_VALID_PASSWORD = "newValidPassword123"
    public static final String ANY_PUBLIC_ID = "da3da28b-ab50-4929-910c-afd750d2a453"
    public static final String ACTIVATION_CODE = "123456"
    public static final String RESET_PASSWORD_TOKEN = "da3da28b-ab50-4929-910c-afd750d2a453"

    static UserEntity anyUserEntity(creationDate = LocalDateTime.now()) {
        var entity = new UserEntity(VALID_EMAIL, ENCODED_PASSWORD, creationDate)
        entity.active = true
        return entity
    }

    static UserTO anyUserTO() {
        return new UserTO(ANY_PUBLIC_ID, VALID_EMAIL, "")
    }

    static UserRegistrationTO anyUserRegistrationTO() {
        return getUserRegistrationTO(VALID_EMAIL, VALID_PASSWORD, VALID_PASSWORD)
    }

    static UserResetPasswordTO anyUserResetPasswordTO() {
        return new UserResetPasswordTO(RESET_PASSWORD_TOKEN, NEW_VALID_PASSWORD, NEW_VALID_PASSWORD)
    }

    static UserRegistrationTO getUserRegistrationTO(String email, String password, String passwordRetyped) {
        return new UserRegistrationTO(email, password, passwordRetyped)
    }

    static PageQuery anyPageQuery() {
        new PageQuery(20, 0)
    }

    static PageCriteriaTO anyPageCriteriaTO() {
        new PageCriteriaTO(1)
    }

    static CriteriaTO<UserFilterTO> anyUserCriteriaTO() {
        new CriteriaTO(anyPageCriteriaTO(), anyUserFilterTO())
    }

    static UserFilterTO anyUserFilterTO() {
        new UserFilterTO(null, null)
    }

    static CriteriaQuery<UserFilterTO> anyUserCriteriaQuery() {
        new CriteriaQuery(anyPageQuery(), anyUserFilterTO())
    }
}
