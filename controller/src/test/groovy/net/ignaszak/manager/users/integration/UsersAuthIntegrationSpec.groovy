package net.ignaszak.manager.users.integration

import com.github.tomakehurst.wiremock.WireMockServer
import net.ignaszak.manager.commons.messaging.message.EmailMessage
import net.ignaszak.manager.commons.rest.response.DataResponse
import net.ignaszak.manager.users.rest.request.UserActivationRequest
import net.ignaszak.manager.users.rest.request.UserRegistrationRequest
import net.ignaszak.manager.users.rest.request.UserResetPasswordRequest
import net.ignaszak.manager.users.rest.request.UserResetPasswordTokenRequest
import net.ignaszak.manager.users.rest.response.UserResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.client.RestTemplate

import java.sql.Time
import java.time.LocalDateTime
import java.time.LocalTime

import static net.ignaszak.manager.commons.error.AuthError.AUT_AUTHENTICATION
import static net.ignaszak.manager.commons.error.AuthError.AUT_INVALID_TOKEN
import static net.ignaszak.manager.commons.test.helper.UserHelper.ANY_EMAIL
import static net.ignaszak.manager.users.TestConstants.ANY_ENCRYPTED_PASSWORD
import static net.ignaszak.manager.users.TestConstants.ANY_PASSWORD
import static net.ignaszak.manager.users.TestConstants.INVALID_EMAIL
import static net.ignaszak.manager.users.TestConstants.VALID_PASSWORD
import static net.ignaszak.manager.users.service.error.UserError.USR_ACTIVATION
import static net.ignaszak.manager.users.service.error.UserError.USR_ACTIVE
import static net.ignaszak.manager.users.service.error.UserError.USR_INVALID_ACTIVATION_CODE
import static net.ignaszak.manager.users.service.error.UserError.USR_INVALID_EMAIL
import static net.ignaszak.manager.users.service.error.UserError.USR_INVALID_PASSWORD
import static net.ignaszak.manager.users.service.error.UserError.USR_INVALID_RESET_PASSWORD_TOKEN
import static net.ignaszak.manager.users.service.error.UserError.USR_USER_EXISTS
import static net.ignaszak.manager.users.service.error.UserError.USR_USER_NOT_FOUND
import static org.springframework.http.HttpStatus.BAD_REQUEST
import static org.springframework.http.HttpStatus.CREATED
import static org.springframework.http.HttpStatus.FORBIDDEN
import static org.springframework.http.HttpStatus.NOT_FOUND
import static org.springframework.http.HttpStatus.OK
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED
import static org.springframework.http.MediaType.APPLICATION_JSON
import static com.github.tomakehurst.wiremock.client.WireMock.*

class UsersAuthIntegrationSpec extends IntegrationSpecification {

    private static final int MOCK_SERVER_PORT = 8111

    private static final String ANY_ACTIVATION_CODE = "123456"
    private static final String ANY_RESET_PASSWORD_TOKEN = "f75a505e-0a63-464d-9211-fdd17693b00c"

    private static final LocalDateTime FUTURE_LOCAL_DATE_TIME = LocalDateTime.now().plusDays(1)

    @DynamicPropertySource
    private static void postgresqlProperties(DynamicPropertyRegistry registry) {
        registry.add("manager.server.gatewayUri", () -> "http://localhost:${MOCK_SERVER_PORT}")
    }

    @Autowired
    private RestTemplate productionRestTemplate

    @SuppressWarnings("all")
    def "register user"() {
        given:
        def request = new UserRegistrationRequest(ANY_EMAIL, VALID_PASSWORD, VALID_PASSWORD)
        def connection = newRabbitMqConnection()
        def channel = newQueue(connection, managerProperties.jms.queues.email)

        when:
        def responseEntity = restTemplateFacade.post("/api/auth/register", request)

        then:
        responseEntity.statusCode == CREATED
        responseEntity.body == null

        and:
        def stmt = CONNECTION.prepareStatement("SELECT * FROM user_account WHERE email = ?")
        stmt.setString(1, ANY_EMAIL)
        def resultSet = stmt.executeQuery()
        resultSet.next()
        !resultSet.getString("public_id").isBlank()
        !resultSet.getBoolean("is_active")
        !resultSet.getString("activation_code").isBlank()

        and:
        def emailMessage = getMessage(channel, managerProperties.jms.queues.email) as EmailMessage
        emailMessage.to == ANY_EMAIL
        emailMessage.body.contains(resultSet.getString("activation_code"))

        cleanup:
        channel.close()
        connection.close()
    }

    def "could not register if email already exists"() {
        given:
        def user = userBuilder.new()
        insert(user)
        def request = new UserRegistrationRequest(user.email, ANY_PASSWORD, ANY_PASSWORD)

        when:
        def responseEntity = restTemplateFacade.post("/api/auth/register", request)

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, USR_USER_EXISTS)
    }

    def "could not register user with invalid data"() {
        given:
        def request = new UserRegistrationRequest(INVALID_EMAIL, "other password", ANY_PASSWORD)

        when:
        def responseEntity = restTemplateFacade.post("/api/auth/register", request)

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, USR_INVALID_PASSWORD, USR_INVALID_EMAIL)
    }

    def "login user"() {
        given:
        def user = userBuilder.new()
        insert(user)
        def request = getLoginRequest(user.email, ANY_PASSWORD)
        def mockServer = initMockTokensServer()
        mockServer.stubFor(post(urlEqualTo("/v1/tokens/api/auth/register")).willReturn(noContent()))

        when:
        def responseEntity = restTemplate.postForEntity("http://localhost:${port}/api/auth/login", request, Object.class) as ResponseEntity<DataResponse<Map<String, String>>>

        then:
        responseEntity.statusCode == OK
        responseEntity.headers.get("Authorization").get(0).startsWith("Bearer")
        responseEntity.body.data.publicId == user.publicId
        responseEntity.body.data.email == user.email

        cleanup:
        mockServer.stop()
    }

    def "error while user login when tokens service fails"() {
        given:
        def user = userBuilder.new()
        insert(user)
        def request = getLoginRequest(user.email, ANY_PASSWORD)
        def mockServer = initMockTokensServer()
        mockServer.stubFor(post(urlEqualTo("/v1/tokens/api/auth/register")).willReturn(badRequest()))

        when:
        def responseEntity = restTemplate.postForEntity("http://localhost:${port}/api/auth/login", request, Object.class) as ResponseEntity<DataResponse<UserResponse>>

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, AUT_AUTHENTICATION)

        cleanup:
        mockServer.stop()
    }

    def "activate user"() {
        given:
        insert(
                userBuilder.newLogged()
                        .setIsActive(false)
                        .setActivationCode(ANY_ACTIVATION_CODE)
                        .setActivationCodeExpiration(FUTURE_LOCAL_DATE_TIME)
        )
        def mockServer = initMockTokensServer()
        mockServer.stubFor(post(urlEqualTo("/v1/tokens/api/auth/activate")).willReturn(noContent()))
        def request = new UserActivationRequest(ANY_ACTIVATION_CODE)

        when:
        def responseEntity = restTemplateAuthFacade.post("/api/auth/activate", request)

        then:
        assertNoContent(responseEntity)

        cleanup:
        mockServer.stop()
    }

    def "error while activation user when code is expired"() {
        given:
        insert(
                userBuilder.new()
                        .setIsActive(false)
                        .setActivationCode(ANY_ACTIVATION_CODE)
                        .setActivationCodeExpiration(FUTURE_LOCAL_DATE_TIME.minusDays(1))
        )
        def request = new UserActivationRequest(ANY_ACTIVATION_CODE)

        when:
        def responseEntity = restTemplateAuthFacade.post("/api/auth/activate", request)

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, USR_INVALID_ACTIVATION_CODE)
    }

    def "error while activation user when code is invalid"() {
        given:
        insert(
                userBuilder.new()
                        .setIsActive(false)
                        .setActivationCode(ANY_ACTIVATION_CODE)
                        .setActivationCodeExpiration(FUTURE_LOCAL_DATE_TIME)
        )
        def request = new UserActivationRequest("invalid activation code")

        when:
        def responseEntity = restTemplateAuthFacade.post("/api/auth/activate", request)

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, USR_INVALID_ACTIVATION_CODE)
    }

    def "error while activation user when user is already active"() {
        given:
        insert(userBuilder.new())
        def request = new UserActivationRequest(ANY_ACTIVATION_CODE)

        when:
        def responseEntity = restTemplateAuthFacade.post("/api/auth/activate", request)

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, USR_INVALID_ACTIVATION_CODE)
    }

    def "error while activation user when user is not authenticated"() {
        given:
        insert(
                userBuilder.new()
                        .setIsActive(false)
                        .setActivationCode(ANY_ACTIVATION_CODE)
                        .setActivationCodeExpiration(FUTURE_LOCAL_DATE_TIME)
        )
        def request = new UserActivationRequest(ANY_ACTIVATION_CODE)

        when:
        def responseEntity = restTemplateFacade.post("/api/auth/activate", request)

        then:
        assertErrorResponse(responseEntity, FORBIDDEN, AUT_INVALID_TOKEN)
    }

    def "error while activation user when user tokens service returns an error"() {
        given:
        insert(
                userBuilder.newLogged()
                        .setIsActive(false)
                        .setActivationCode(ANY_ACTIVATION_CODE)
                        .setActivationCodeExpiration(FUTURE_LOCAL_DATE_TIME)
        )
        def mockServer = initMockTokensServer()
        mockServer.stubFor(post(urlEqualTo("/v1/tokens/api/auth/activate")).willReturn(badRequest()))
        def request = new UserActivationRequest(ANY_ACTIVATION_CODE)

        when:
        def responseEntity = restTemplateAuthFacade.post("/api/auth/activate", request)

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, USR_ACTIVATION)

        cleanup:
        mockServer.stop()
    }

    def "error while activation user when user is not activated after activation period"() {
        given:
        insert(
                userBuilder.newLogged()
                        .setIsActive(false)
                        .setActivationCode(ANY_ACTIVATION_CODE)
                        .setActivationCodeExpiration(FUTURE_LOCAL_DATE_TIME)
                        .setCreationDate(LocalDateTime.now().minusDays(2))
        )
        def request = new UserActivationRequest(ANY_ACTIVATION_CODE)

        when:
        def responseEntity = restTemplateAuthFacade.post("/api/auth/activate", request)

        then:
        assertErrorResponse(responseEntity, NOT_FOUND, USR_USER_NOT_FOUND)
    }

    @SuppressWarnings("all")
    def "send activation code"() {
        given:
        def user = userBuilder.newLogged().setIsActive(false)
        insert(user)
        def request = new UserActivationRequest(ANY_ACTIVATION_CODE)
        def connection = newRabbitMqConnection()
        def channel = newQueue(connection, managerProperties.jms.queues.email)

        when:
        def responseEntity = restTemplateAuthFacade.post("/api/auth/sendActivationCode", request)

        then:
        assertNoContent(responseEntity)

        and:
        def stmt = CONNECTION.prepareStatement("SELECT * FROM user_account WHERE public_id = ?")
        stmt.setString(1, user.publicId)
        def resultSet = stmt.executeQuery()
        resultSet.next()
        !resultSet.getBoolean("is_active")
        !resultSet.getString("activation_code").isBlank()
        resultSet.getTime("activation_code_expiration").after(Time.valueOf(LocalTime.now()))

        and:
        def emailMessage = getMessage(channel, managerProperties.jms.queues.email) as EmailMessage
        emailMessage.to == user.email
        emailMessage.body.contains(resultSet.getString("activation_code"))

        cleanup:
        channel.close()
        connection.close()
    }

    def "error while sending activation code when user is not authorized"() {
        given:
        insert(
                userBuilder.new()
                        .setIsActive(false)
        )
        def request = new UserActivationRequest(ANY_ACTIVATION_CODE)

        when:
        def responseEntity = restTemplateFacade.post("/api/auth/sendActivationCode", request)

        then:
        assertErrorResponse(responseEntity, FORBIDDEN, AUT_INVALID_TOKEN)
    }

    def "error while sending activation code when user is active"() {
        given:
        insert(userBuilder.newLogged())
        def request = new UserActivationRequest(ANY_ACTIVATION_CODE)

        when:
        def responseEntity = restTemplateAuthFacade.post("/api/auth/sendActivationCode", request)

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, USR_ACTIVE)
    }

    @SuppressWarnings("all")
    def "send reset password token for authorized active user"() {
        given:
        def user = userBuilder.new()
        insert(user)
        def request = new UserResetPasswordTokenRequest(user.email)
        def connection = newRabbitMqConnection()
        def channel = newQueue(connection, managerProperties.jms.queues.email)

        when:
        def responseEntity = restTemplateAuthFacade.post("/api/auth/sendResetPasswordToken", request)

        then:
        assertNoContent(responseEntity)

        and:
        int fails = 0
        boolean succeed = false
        def resetPasswordToken = ""
        do {
            def stmt = CONNECTION.prepareStatement("SELECT * FROM user_account WHERE public_id = ?")
            stmt.setString(1, user.publicId)
            def resultSet = stmt.executeQuery()
            resultSet.next()
            resetPasswordToken = resultSet.getString("reset_password_token")
            if (resetPasswordToken != null) {
                resultSet.getBoolean("is_active")
                !resetPasswordToken.isBlank()
                resultSet.getTime("reset_password_token_expiration").after(Time.valueOf(LocalTime.now()))
                succeed = true
            } else {
                fails++
                sleep(1000)
            }
        } while (!succeed && fails < 3)

        and:
        def emailMessage = getMessage(channel, managerProperties.jms.queues.email) as EmailMessage
        emailMessage.to == user.email
        emailMessage.body.contains(resetPasswordToken)

        cleanup:
        channel.close()
        connection.close()
    }

    @SuppressWarnings("all")
    def "send reset password token for unauthorized active user"() {
        given:
        def user = userBuilder.new()
        insert(user)
        def request = new UserResetPasswordTokenRequest(user.email)
        def connection = newRabbitMqConnection()
        def channel = newQueue(connection, managerProperties.jms.queues.email)

        when:
        def responseEntity = restTemplateFacade.post("/api/auth/sendResetPasswordToken", request)

        then:
        assertNoContent(responseEntity)

        and:
        int fails = 0
        boolean succeed = false
        def resetPasswordToken = ""
        do {
            def stmt = CONNECTION.prepareStatement("SELECT * FROM user_account WHERE public_id = ?")
            stmt.setString(1, user.publicId)
            def resultSet = stmt.executeQuery()
            resultSet.next()
            resetPasswordToken = resultSet.getString("reset_password_token")
            if (resetPasswordToken != null) {
                resultSet.getBoolean("is_active")
                !resetPasswordToken.isBlank()
                resultSet.getTime("reset_password_token_expiration").after(Time.valueOf(LocalTime.now()))
                succeed = true
            } else {
                fails++
                sleep(1000)
            }
        } while (!succeed && fails < 3)

        and:
        def emailMessage = getMessage(channel, managerProperties.jms.queues.email) as EmailMessage
        emailMessage.to == user.email
        emailMessage.body.contains(resetPasswordToken)

        cleanup:
        channel.close()
        connection.close()
    }

    def "error while sending reset password token for inactive user"() {
        given:
        def user = userBuilder.new().setIsActive(false)
        insert(user)
        def request = new UserResetPasswordTokenRequest(ANY_EMAIL)

        when:
        def responseEntity = restTemplateAuthFacade.post("/api/auth/sendResetPasswordToken", request)

        then:
        assertNoContent(responseEntity)

        and:
        assertResetPasswordTokenNotSet(user.publicId)
    }

    def "error while sending reset password token with invalid email"() {
        given:
        def user = userBuilder.new()
        insert(user)
        def request = new UserResetPasswordTokenRequest(INVALID_EMAIL)

        when:
        def responseEntity = restTemplateFacade.post("/api/auth/sendResetPasswordToken", request)

        then:
        assertNoContent(responseEntity)

        and:
        assertResetPasswordTokenNotSet(user.publicId)
    }

    @SuppressWarnings("all")
    def "reset password for authorized active user"() {
        given:
        def user = userBuilder.new()
                .setResetPasswordToken(ANY_RESET_PASSWORD_TOKEN)
                .setResetPasswordTokenExpiration(FUTURE_LOCAL_DATE_TIME)
        insert(user)
        def request = new UserResetPasswordRequest("newPassword123", "newPassword123")

        when:
        def responseEntity = restTemplateAuthFacade.post("/api/auth/resetPassword/${ANY_RESET_PASSWORD_TOKEN}", request)

        then:
        assertNoContent(responseEntity)

        and:
        def stmt = CONNECTION.prepareStatement("SELECT password FROM user_account WHERE public_id = ?")
        stmt.setString(1, user.publicId)
        def resultSet = stmt.executeQuery()
        resultSet.next()
        resultSet.getString("password") != ANY_ENCRYPTED_PASSWORD
    }

    @SuppressWarnings("all")
    def "reset password for unauthorized active user"() {
        given:
        def user = userBuilder.new()
                .setResetPasswordToken(ANY_RESET_PASSWORD_TOKEN)
                .setResetPasswordTokenExpiration(FUTURE_LOCAL_DATE_TIME)
        insert(user)
        def request = new UserResetPasswordRequest("newPassword123", "newPassword123")

        when:
        def responseEntity = restTemplateFacade.post("/api/auth/resetPassword/${ANY_RESET_PASSWORD_TOKEN}", request)

        then:
        assertNoContent(responseEntity)

        and:
        def stmt = CONNECTION.prepareStatement("SELECT password FROM user_account WHERE public_id = ?")
        stmt.setString(1, user.publicId)
        def resultSet = stmt.executeQuery()
        resultSet.next()
        resultSet.getString("password") != ANY_ENCRYPTED_PASSWORD
    }

    def "error while resetting password when token has expired"() {
        given:
        def user = userBuilder.new()
                .setResetPasswordToken(ANY_RESET_PASSWORD_TOKEN)
                .setResetPasswordTokenExpiration(LocalDateTime.now().minusMinutes(1))
        insert(user)
        def request = new UserResetPasswordRequest("newPassword123", "newPassword123")

        when:
        def responseEntity = restTemplateFacade.post("/api/auth/resetPassword/${ANY_RESET_PASSWORD_TOKEN}", request)

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, USR_INVALID_RESET_PASSWORD_TOKEN)

        and:
        assertPasswordHasNotChanged(user.publicId)
    }

    def "error while resetting password with invalid token"() {
        given:
        def user = userBuilder.new()
                .setResetPasswordToken(ANY_RESET_PASSWORD_TOKEN)
                .setResetPasswordTokenExpiration(FUTURE_LOCAL_DATE_TIME)
        insert(user)
        def request = new UserResetPasswordRequest("newPassword123", "newPassword123")

        when:
        def responseEntity = restTemplateFacade.post("/api/auth/resetPassword/invalid-reset-token", request)

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, USR_INVALID_RESET_PASSWORD_TOKEN)

        and:
        assertPasswordHasNotChanged(user.publicId)
    }

    def "error while resetting password with invalid password"(String password, String passwordRetyped) {
        given:
        def user = userBuilder.new()
                .setResetPasswordToken(ANY_RESET_PASSWORD_TOKEN)
                .setResetPasswordTokenExpiration(FUTURE_LOCAL_DATE_TIME)
        insert(user)
        def request = new UserResetPasswordRequest(password, passwordRetyped)

        when:
        def responseEntity = restTemplateFacade.post("/api/auth/resetPassword/${ANY_RESET_PASSWORD_TOKEN}", request)

        then:
        assertErrorResponse(responseEntity, BAD_REQUEST, USR_INVALID_PASSWORD)

        and:
        assertPasswordHasNotChanged(user.publicId)

        where:
        password  | passwordRetyped
        "invalid" | "invalid"
        ""        | "newPassword123"
    }

    @SuppressWarnings("all")
    private static void assertResetPasswordTokenNotSet(String publicId) {
        def stmt = CONNECTION.prepareStatement("SELECT * FROM user_account WHERE public_id = ?")
        stmt.setString(1, publicId)
        def resultSet = stmt.executeQuery()
        resultSet.next()
        assert resultSet.getString("reset_password_token") == null
        assert resultSet.getTime("reset_password_token_expiration") == null
    }

    private static WireMockServer initMockTokensServer() {
        def mockServer = new WireMockServer(MOCK_SERVER_PORT)
        mockServer.start()

        mockServer.stubFor(
                post(urlEqualTo("/v1/tokens/oauth2/token"))
                        .willReturn(ok().withHeader("Content-Type", APPLICATION_JSON.toString()).withBody(anyOauthTokenResponse()))
        )

        mockServer
    }

    private static String anyOauthTokenResponse() {
        """
        {
          "access_token": "access token code",
          "scope": "validate register",
          "token_type": "Bearer",
          "expires_in": 299
        }
        """
    }

    private HttpEntity getLoginRequest(String username, String password) {
        def body = new LinkedMultiValueMap<String, String>()
        body.add("username", username)
        body.add("password", password)
        def headers = new HttpHeaders()
        headers.setContentType(APPLICATION_FORM_URLENCODED)
        new HttpEntity(body, headers)
    }
}
