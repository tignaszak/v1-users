package net.ignaszak.manager.users.security.mapper;

import net.ignaszak.manager.commons.jwt.to.JwtUserTO;
import net.ignaszak.manager.users.rest.response.UserResponse;
import net.ignaszak.manager.users.security.model.UserPrincipalModel;
import org.mapstruct.Mapper;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Primary
@Component
@Mapper(componentModel = "spring")
public interface UserPrincipalMapper {
    UserResponse userPrincipalModelToUserResponse(UserPrincipalModel userPrincipalModel);
    JwtUserTO userPrincipalModelToJwtUserTO(UserPrincipalModel userPrincipalModel);
}
