package net.ignaszak.manager.users.mapper;

import lombok.AllArgsConstructor;
import net.ignaszak.manager.commons.rest.page.PageRequest;
import net.ignaszak.manager.commons.restcriteria.to.CriteriaTO;
import net.ignaszak.manager.users.rest.filter.UserFilterRequest;
import net.ignaszak.manager.users.to.UserFilterTO;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
class UserRestCriteriaMapper implements RestCriteriaMapper<UserFilterRequest, UserFilterTO> {

    private final net.ignaszak.manager.commons.spring.restcriteria.mapper.RestCriteriaMapper restCriteriaMapper;

    @Override
    public net.ignaszak.manager.commons.spring.restcriteria.mapper.RestCriteriaMapper getCommonRestCriteriaMapper() {
        return restCriteriaMapper;
    }

    @Override
    public CriteriaTO<UserFilterTO> pageRequestAndFilterRequestToCriteriaTO(final PageRequest pageRequest, final UserFilterRequest filterRequest) {
        return new CriteriaTO<>(
            restCriteriaMapper.pageRequestToPageCriteriaTO(pageRequest),
            userFilterRequestToUserFilterTO(filterRequest)
        );
    }

    private UserFilterTO userFilterRequestToUserFilterTO(UserFilterRequest request) {
        return new UserFilterTO(
            restCriteriaMapper.stringOperatorToStringOperatorTO(request.getPublicId()),
            restCriteriaMapper.stringOperatorToStringOperatorTO(request.getEmail())
        );
    }
}
