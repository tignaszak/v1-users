package net.ignaszak.manager.users.integration.builder

class UserBuilderService {
    private final String encodedPassword

    UserBuilderService(final String encodedPassword) {
        this.encodedPassword = encodedPassword
    }

    UserBuilder "new"() {
        UserBuilder.new(encodedPassword)
    }

    UserBuilder "newLogged"() {
        UserBuilder.newLogged(encodedPassword)
    }
}
