package net.ignaszak.manager.users.to;

public record UserRegistrationTO(String email, String password, String passwordRepeated) { }
