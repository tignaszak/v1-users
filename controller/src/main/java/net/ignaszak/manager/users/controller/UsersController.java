package net.ignaszak.manager.users.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import net.ignaszak.manager.commons.openapi.annotations.ApiOKResponse;
import net.ignaszak.manager.commons.openapi.annotations.ApiUserPublicIdParameter;
import net.ignaszak.manager.commons.openapi.annotations.UserJwtSecurityRequirement;
import net.ignaszak.manager.commons.rest.page.PageRequest;
import net.ignaszak.manager.commons.rest.response.DataResponse;
import net.ignaszak.manager.commons.rest.response.ListResponse;
import net.ignaszak.manager.commons.restcriteria.filtering.annotation.ParameterFilter;
import net.ignaszak.manager.commons.user.annotation.UserPrincipal;
import net.ignaszak.manager.commons.user.to.UserPrincipalTO;
import net.ignaszak.manager.users.facade.ServiceFacade;
import net.ignaszak.manager.users.openapi.ApiUserFilterParameter;
import net.ignaszak.manager.users.rest.filter.UserFilterRequest;
import net.ignaszak.manager.users.rest.request.UserUpdateRequest;
import net.ignaszak.manager.users.rest.response.UserResponse;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping
@AllArgsConstructor
@Tag(name = "User", description = "User operations")
public class UsersController {

    private final ServiceFacade serviceFacade;

    @GetMapping
    @Operation(summary = "Get users list")
    @UserJwtSecurityRequirement
    @ApiOKResponse
    public ListResponse<UserResponse> list(@ParameterObject PageRequest pageRequest,
                                           @ApiUserFilterParameter @ParameterFilter UserFilterRequest filter) {
        return serviceFacade.getUserList(pageRequest, filter);
    }

    @GetMapping("/me")
    @Operation(summary = "Get logged user")
    @UserJwtSecurityRequirement
    @ApiOKResponse
    public DataResponse<UserResponse> me(@UserPrincipal UserPrincipalTO userPrincipalTO) {
        return serviceFacade.getUserByPublicId(userPrincipalTO.getPublicId());
    }

    @GetMapping("/{publicId}")
    @Operation(summary = "Get user by public id")
    @UserJwtSecurityRequirement
    @ApiOKResponse
    public DataResponse<UserResponse> getUserByPublicId(@ApiUserPublicIdParameter @PathVariable String publicId) {
        return serviceFacade.getUserByPublicId(publicId);
    }

    @PutMapping("/{publicId}")
    @Operation(summary = "Update user")
    @UserJwtSecurityRequirement
    @ApiOKResponse
    public DataResponse<UserResponse> update(@ApiUserPublicIdParameter @PathVariable String publicId,
                                             @RequestBody UserUpdateRequest userUpdateRequest) {
        return serviceFacade.updateUser(publicId, userUpdateRequest);
    }
}
