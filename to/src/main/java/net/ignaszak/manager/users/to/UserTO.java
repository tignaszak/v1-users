package net.ignaszak.manager.users.to;

public record UserTO(String publicId, String email, String activationCode) {}
