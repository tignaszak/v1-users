package net.ignaszak.manager.users.rest.filter;

import lombok.Value;
import net.ignaszak.manager.commons.rest.filter.StringOperator;

@Value
@SuppressWarnings("all")
/* Gson does not supports java records */
public class UserFilterRequest {
    StringOperator publicId;
    StringOperator email;
}
