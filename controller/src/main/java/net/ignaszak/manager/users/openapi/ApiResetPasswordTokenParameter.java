package net.ignaszak.manager.users.openapi;

import io.swagger.v3.oas.annotations.Parameter;

import static io.swagger.v3.oas.annotations.enums.ParameterIn.PATH;

@Parameter(
        description = "Reset password token",
        in = PATH,
        required = true,
        example = "f75a505e-0a63-464d-9211-fdd17693b00c"
)
public @interface ApiResetPasswordTokenParameter {}
