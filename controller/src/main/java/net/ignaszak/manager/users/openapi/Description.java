package net.ignaszak.manager.users.openapi;

import static net.ignaszak.manager.commons.openapi.description.CriteriaDescription.PUBLIC_ID_OPERATORS;
import static net.ignaszak.manager.commons.openapi.description.CriteriaDescription.TEXT_OPERATORS;

final public class Description {

    public static final String USER_FILTER_DESCRIPTION = "Available parameters:" +
        "\n - <code>publicId</code> - project public id, " + PUBLIC_ID_OPERATORS +
        "\n - <code>email</code> - project title, " + TEXT_OPERATORS;

    private Description() {}
}
