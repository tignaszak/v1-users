package net.ignaszak.manager.users.integration

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import net.ignaszak.manager.users.integration.builder.UserBuilder
import net.ignaszak.manager.users.integration.builder.UserBuilderService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.PostgreSQLContainer
import spock.lang.Shared

import java.sql.Connection
import java.sql.Timestamp
import java.time.LocalDateTime

import static net.ignaszak.manager.users.TestConstants.ANY_PASSWORD

abstract class IntegrationSpecification extends net.ignaszak.manager.commons.test.specification.IntegrationSpecification {

    @Autowired
    private PasswordEncoder passwordEncoder

    protected static UserBuilderService userBuilder

    @Shared
    protected static final PostgreSQLContainer POSTGRESQL_CONTAINER = new PostgreSQLContainer("postgres:15.0")
            .withDatabaseName("integration-tests-db")
            .withUsername("sa")
            .withPassword("sa")

    protected static Connection CONNECTION

    private static String ENCODED_PASSWORD

    def setup() {
        if (ENCODED_PASSWORD == null) {
            ENCODED_PASSWORD = passwordEncoder.encode(ANY_PASSWORD)
            userBuilder = new UserBuilderService(ENCODED_PASSWORD)
        }
    }

    def cleanup() {
        clearUser()
    }

    def cleanupSpec() {
        POSTGRESQL_CONTAINER.stop()
    }

    @DynamicPropertySource
    private static void postgresqlProperties(DynamicPropertyRegistry registry) {
        buildConnection(registry)
    }

    @SuppressWarnings("all")
    protected static boolean assertPasswordHasNotChanged(String publicId) {
        def stmt = CONNECTION.prepareStatement("SELECT password FROM user_account WHERE public_id = ?")
        stmt.setString(1, publicId)
        def resultSet = stmt.executeQuery()
        resultSet.next()
        return resultSet.getString("password") == ENCODED_PASSWORD
    }

    @SuppressWarnings("all")
    protected static void insert(UserBuilder builder) {
        def stmt = CONNECTION.prepareStatement("""
            INSERT INTO user_account
                (public_id, email, password, is_active, activation_code, activation_code_expiration, reset_password_token, reset_password_token_expiration, creation_date)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)
        """)
        stmt.setString(1, builder.publicId)
        stmt.setString(2, builder.email)
        stmt.setString(3, builder.password)
        stmt.setBoolean(4, builder.isActive)
        stmt.setString(5, builder.activationCode)
        stmt.setTimestamp(6, Optional.ofNullable(builder.activationCodeExpiration).map(Timestamp::valueOf).orElse(null))
        stmt.setString(7, builder.resetPasswordToken)
        stmt.setTimestamp(8, Optional.ofNullable(builder.resetPasswordTokenExpiration).map(Timestamp::valueOf).orElse(null))
        stmt.setTimestamp(9, Optional.ofNullable(builder.creationDate).map(Timestamp::valueOf).orElse(Timestamp.valueOf(LocalDateTime.now())))
        stmt.execute()
    }

    @SuppressWarnings("all")
    protected static void clearUser() {
        CONNECTION.prepareStatement("TRUNCATE user_account").execute()
    }

    protected final static buildConnection(DynamicPropertyRegistry registry) {
        POSTGRESQL_CONTAINER.start()

        registry.add("spring.datasource.url", POSTGRESQL_CONTAINER::getJdbcUrl)
        registry.add("spring.datasource.username", POSTGRESQL_CONTAINER::getUsername)
        registry.add("spring.datasource.password", POSTGRESQL_CONTAINER::getPassword)

        HikariConfig hikariConfig = new HikariConfig()
        hikariConfig.setJdbcUrl(POSTGRESQL_CONTAINER.getJdbcUrl())
        hikariConfig.setUsername(POSTGRESQL_CONTAINER.getUsername())
        hikariConfig.setPassword(POSTGRESQL_CONTAINER.getPassword())

        CONNECTION = new HikariDataSource(hikariConfig).getConnection()
    }
}
