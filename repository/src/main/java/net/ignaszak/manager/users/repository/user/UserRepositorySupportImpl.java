package net.ignaszak.manager.users.repository.user;

import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Projections;
import net.ignaszak.manager.commons.restcriteria.query.CriteriaQuery;
import net.ignaszak.manager.commons.utils.datetime.IDateTimeProvider;
import net.ignaszak.manager.users.dto.user.UserDTO;
import net.ignaszak.manager.users.entity.QUserEntity;
import net.ignaszak.manager.users.entity.UserEntity;
import net.ignaszak.manager.users.to.UserFilterTO;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import java.time.Period;
import java.util.LinkedHashSet;
import java.util.Set;

import static com.querydsl.core.types.dsl.Expressions.*;
import static java.util.Collections.unmodifiableSet;
import static net.ignaszak.manager.commons.querydsl.criteria.CriteriaHelper.publicIdCriteria;
import static net.ignaszak.manager.commons.querydsl.criteria.CriteriaHelper.textCriteria;

@Repository
class UserRepositorySupportImpl extends QuerydslRepositorySupport implements UserRepositorySupport {

    private static final QUserEntity USER = QUserEntity.userEntity;

    private final IDateTimeProvider dateTimeProvider;

    UserRepositorySupportImpl(final IDateTimeProvider dateTimeProvider) {
        super(UserEntity.class);
        this.dateTimeProvider = dateTimeProvider;
    }

    @Override
    public Set<UserDTO> findUserDTOSet(final CriteriaQuery<UserFilterTO> criteriaQuery) {
        final var users = new LinkedHashSet<>(getUserDTOSetQueryResult(criteriaQuery).getResults());

        return unmodifiableSet(users);
    }

    @Override
    public long countUserDTOSet(final CriteriaQuery<UserFilterTO> criteriaQuery) {
        return getUserDTOSetQueryResult(criteriaQuery).getTotal();
    }

    @Override
    public void deleteInactiveUsersByPeriod(final Period period) {
        final var periodDate = dateTimeProvider.getNow().minus(period);

        delete(USER)
            .where(
                USER.isActive.isFalse()
                    .and(USER.creationDate.before(asDateTime(periodDate)))
            )
            .execute();
    }

    @Override
    public boolean isUserExpiredAfterPeriod(final String publicId, final Period period) {
        final var count = from(USER)
            .where(USER.creationDate.before(dateTimeProvider.getNow().minus(period)))
            .fetchCount();

        return count == 1;
    }

    private QueryResults<UserDTO> getUserDTOSetQueryResult(final CriteriaQuery<UserFilterTO> criteriaQuery) {
        final var query = from(USER)
            .select(
                Projections.constructor(
                    UserDTO.class,
                    USER.publicId,
                    USER.email
                )
            )
            .where(USER.isActive.isTrue());

        publicIdCriteria(query, criteriaQuery.getFilterTO().publicId(), () -> USER.publicId);
        textCriteria(query, criteriaQuery.getFilterTO().email(), () -> USER.email);

        return query
            .orderBy(USER.email.asc())
            .limit(criteriaQuery.getPageQuery().getSize())
            .offset(criteriaQuery.getPageQuery().getOffset())
            .fetchResults();
    }
}

