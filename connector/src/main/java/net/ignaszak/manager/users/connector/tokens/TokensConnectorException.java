package net.ignaszak.manager.users.connector.tokens;

public class TokensConnectorException extends Exception {
    TokensConnectorException(String message) {
        super(message);
    }
}
