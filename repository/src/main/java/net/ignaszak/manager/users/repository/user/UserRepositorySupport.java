package net.ignaszak.manager.users.repository.user;

import net.ignaszak.manager.commons.restcriteria.query.CriteriaQuery;
import net.ignaszak.manager.users.dto.user.UserDTO;
import net.ignaszak.manager.users.to.UserFilterTO;

import java.time.Period;
import java.util.Set;

public interface UserRepositorySupport {
    Set<UserDTO> findUserDTOSet(CriteriaQuery<UserFilterTO> criteriaQuery);
    long countUserDTOSet(CriteriaQuery<UserFilterTO> criteriaQuery);
    void deleteInactiveUsersByPeriod(Period period);
    boolean isUserExpiredAfterPeriod(String publicId, Period period);
}
