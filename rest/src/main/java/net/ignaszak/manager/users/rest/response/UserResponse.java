package net.ignaszak.manager.users.rest.response;

import io.swagger.v3.oas.annotations.media.Schema;
import net.ignaszak.manager.commons.email.annotation.EmailTo;

import static net.ignaszak.manager.users.rest.openapi.ApiConstants.EMAIL_ADDRESS;
import static net.ignaszak.manager.users.rest.openapi.ApiConstants.USER_PUBLIC_ID;

@Schema(title = "User response")
public record UserResponse(
        @Schema(description = USER_PUBLIC_ID) String publicId,
        @Schema(description = EMAIL_ADDRESS) @EmailTo String email
) {}
