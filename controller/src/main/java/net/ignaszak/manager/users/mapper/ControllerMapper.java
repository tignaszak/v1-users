package net.ignaszak.manager.users.mapper;

import net.ignaszak.manager.commons.email.data.RegistrationData;
import net.ignaszak.manager.users.rest.request.UserRegistrationRequest;
import net.ignaszak.manager.users.rest.request.UserResetPasswordRequest;
import net.ignaszak.manager.users.rest.request.UserUpdateRequest;
import net.ignaszak.manager.users.rest.response.UserResponse;
import net.ignaszak.manager.users.to.*;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
@SuppressWarnings("all")
public interface ControllerMapper {
    UserRegistrationTO userRegistrationRequestToUserRegistrationTO(UserRegistrationRequest userRegistrationRequest);
    UserResponse userTOToUserResponse(UserTO userTO);
    RegistrationData userTOToRegistrationData(UserTO userTO);
    UserResetPasswordTO tokenAndUserResetPasswordRequestToUserResetPasswordTO(String token, UserResetPasswordRequest userResetPasswordRequest);
    UserUpdateTO publicIdAndUserUpdateRequestToUserUpdateTO(String publicId, UserUpdateRequest userUpdateRequest);
    UserResponse userMinimumTOToUserResponse(UserMinimumTO userMinimumTO);
}
