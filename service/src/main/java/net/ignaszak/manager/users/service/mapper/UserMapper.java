package net.ignaszak.manager.users.service.mapper;

import net.ignaszak.manager.users.dto.user.UserDTO;
import net.ignaszak.manager.users.entity.UserEntity;
import net.ignaszak.manager.users.to.UserMinimumTO;
import net.ignaszak.manager.users.to.UserTO;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface UserMapper {

    default UserTO userEntityToUserTO(UserEntity userEntity) {
        return new UserTO(userEntity.getPublicId(), userEntity.getEmail(), userEntity.getActivationCode());
    }

    UserMinimumTO userDTOToUserMinimumTO(UserDTO userDTO);
}
