package net.ignaszak.manager.users.connector.email;

public interface EmailConnectorInterface {
    void sendResetPasswordEmail(String to, String token);
}
