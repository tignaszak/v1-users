package net.ignaszak.manager.users.scheduler;

import lombok.AllArgsConstructor;
import net.ignaszak.manager.users.facade.ServiceFacade;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
class UserScheduler {

    private final ServiceFacade serviceFacade;

    @Scheduled(cron = "#{appConfigImpl.deleteInactiveUserCronSchedule}")
    void removeInactiveUsers() {
        serviceFacade.deleteInactiveUsersAsync();
    }
}
