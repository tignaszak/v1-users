package net.ignaszak.manager.users.rest.openapi;

public final class ApiConstants {

    public static final String EMAIL_ADDRESS = "User email address";

    public static final String PASSWORD = "User password, minimum eight characters, at least one letter and one digit";
    public static final String PASSWORD_REPEATED = "Password repeated";

    public static final String USER_ACTIVATION_CODE = "User activation code";
    public static final String USER_ACTIVATION_CODE_EXAMPLE = "123456";
    public static final String USER_PUBLIC_ID = "User public id";

    private ApiConstants() {}
}
