package net.ignaszak.manager.users.dto.user;

public record UserDTO(String publicId, String email) {}
