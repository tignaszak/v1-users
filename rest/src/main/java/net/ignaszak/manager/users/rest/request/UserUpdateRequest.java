package net.ignaszak.manager.users.rest.request;

import io.swagger.v3.oas.annotations.media.Schema;

import static net.ignaszak.manager.users.rest.openapi.ApiConstants.PASSWORD;
import static net.ignaszak.manager.users.rest.openapi.ApiConstants.PASSWORD_REPEATED;

@Schema(title = "User update request")
public record UserUpdateRequest(
        @Schema(description = "Old password", required = true) String oldPassword,
        @Schema(description = PASSWORD, required = true) String newPassword,
        @Schema(description = PASSWORD_REPEATED, required = true) String newPasswordRetyped
) {}
