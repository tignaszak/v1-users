package net.ignaszak.manager.users

import lombok.experimental.UtilityClass
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

@UtilityClass
final class TestConstants {

    public static final String PARAM_PASSWORD = "password"
    public static final String PARAM_USERNAME = "username"

    public static final String ANY_PASSWORD = "password1"
    public static final String ANY_ENCRYPTED_PASSWORD = new BCryptPasswordEncoder().encode(ANY_PASSWORD)

    public static final String INVALID_EMAIL = "invalid@email"
    public static final String VALID_EMAIL = "valid@email.com"
    public static final String VALID_PASSWORD = "validPassword123"
}
