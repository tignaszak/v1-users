package net.ignaszak.manager.users.security

import net.ignaszak.manager.users.security.model.UserDetailsModel
import net.ignaszak.manager.users.security.model.UserPrincipalModel
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails

import static net.ignaszak.manager.commons.test.helper.UserHelper.ANY_EMAIL
import static net.ignaszak.manager.commons.test.helper.UserHelper.ANY_PUBLIC_ID
import static net.ignaszak.manager.commons.test.helper.UserHelper.USER_ROLE
import static net.ignaszak.manager.users.TestConstants.ANY_ENCRYPTED_PASSWORD

class TestSecurityHelper {

    static UserDetails anyUserDetails() {
        def userDetails = User.withUsername(ANY_EMAIL)
                .password(ANY_ENCRYPTED_PASSWORD)
                .roles(USER_ROLE)
                .build()

        return new UserDetailsModel(anyUserPrincipalModel(), userDetails)
    }

    static UserPrincipalModel anyUserPrincipalModel() {
        return new UserPrincipalModel(ANY_PUBLIC_ID, ANY_EMAIL, true)
    }
}
