package net.ignaszak.manager.users.security.service;

import net.ignaszak.manager.users.security.model.UserSecurityModel;

public interface UserSecurityService {

    UserSecurityModel getUserSecurityModelByEmail(String email);
}
