package net.ignaszak.manager.users.connector.tokens;

public interface TokensConnectorInterface {
    void register(String jwt, String userPublicId) throws TokensConnectorException;
    void activate(String userPublicId) throws TokensConnectorException;
}
