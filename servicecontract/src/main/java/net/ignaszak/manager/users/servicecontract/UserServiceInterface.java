package net.ignaszak.manager.users.servicecontract;

import net.ignaszak.manager.commons.restcriteria.to.CriteriaTO;
import net.ignaszak.manager.users.to.*;

import java.time.Period;
import java.util.Set;

public interface UserServiceInterface {

    UserTO getUserByEmail(String email);
    UserTO register(UserRegistrationTO userRegistrationTO);
    UserTO getUserByPublicId(String publicId);
    void activate(String publicId, String code);
    String createResetPasswordToken(String email);
    UserTO setActivationCode(String publicId);
    void resetPassword(UserResetPasswordTO userResetPasswordTO);
    UserTO update(UserUpdateTO userUpdateTO);
    Set<UserMinimumTO> getUserSet(CriteriaTO<UserFilterTO> criteriaTO);
    long countUserSet(CriteriaTO<UserFilterTO> criteriaTO);
    void deleteInactiveUsersByPeriod(Period period);
}
