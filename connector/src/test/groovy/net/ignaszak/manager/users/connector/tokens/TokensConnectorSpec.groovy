package net.ignaszak.manager.users.connector.tokens

import net.ignaszak.manager.commons.httpclient.Request
import net.ignaszak.manager.commons.httpclient.Response
import net.ignaszak.manager.commons.httpclient.client.HttpClient
import net.ignaszak.manager.commons.rest.token.TokenActivationRequest
import net.ignaszak.manager.commons.rest.token.TokenRegistrationRequest
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import spock.lang.Specification

import static net.ignaszak.manager.commons.test.ManagerPropertiesTest.managerProperties
import static org.springframework.http.HttpStatus.NO_CONTENT

class TokensConnectorSpec extends Specification {

    private HttpClient httpClientMock = Mock()

    private TokensConnectorInterface subject = new TokensConnectorImpl(httpClientMock, getManagerProperties())

    def "no exception is thrown on successful jwt registration"() {
        given:
        def jwt = anyJwt()
        def userPublicId = anyUserPublicId()
        1 * httpClientMock.send(anyRegistrationRequest()) >> new Response(Optional.empty(), NO_CONTENT)

        when:
        subject.register(jwt, userPublicId)

        then:
        noExceptionThrown()
    }

    def "throw exception when jwt or userPublicId are blank while registration"(String jwt, String userPublicId) {
        when:
        subject.register(jwt, userPublicId)

        then:
        def e = thrown(TokensConnectorException)
        e.message == "Invalid jwt token!"

        where:
        jwt  | userPublicId
        null | null
        " "  | null
        null | " "
        " "  | " "
    }

    def "throw exception when http client returns fail response while registration"() {
        given:
        1 * httpClientMock.send(_) >> new Response(Optional.empty(), HttpStatus.BAD_REQUEST)

        when:
        subject.register(anyJwt(), anyUserPublicId())

        then:
        def e = thrown(TokensConnectorException)
        e.message == "Could not register jwt token! TokenRegistrationRequest(userPublicId=any user public id, jwt=any jwt)"
    }

    def "no exception is thrown on successful activation"() {
        given:
        1 * httpClientMock.send(anyActivationRequest()) >> new Response(Optional.empty(), NO_CONTENT)

        when:
        subject.activate(anyUserPublicId())

        then:
        noExceptionThrown()
    }

    def "throw exception when userPublicId is blank while activation"(String userPublicId) {
        when:
        subject.activate(userPublicId)

        then:
        def e = thrown(TokensConnectorException)
        e.message == "Invalid user public id!"

        where:
        userPublicId | _
        null         | _
        " "          | _
    }

    def "throw exception when http client returns fail response while activation"() {
        given:
        1 * httpClientMock.send(_) >> new Response(Optional.empty(), HttpStatus.BAD_REQUEST)

        when:
        subject.activate(anyUserPublicId())

        then:
        def e = thrown(TokensConnectorException)
        e.message == "Could not activate jwt token! TokenActivationRequest(userPublicId=any user public id)"
    }

    private static Request anyRegistrationRequest() {
        new Request(
                "http://localhost:8080/v1/tokens/api/auth/register",
                HttpMethod.POST,
                new TokenRegistrationRequest(anyUserPublicId(), anyJwt())
        )
    }

    private static Request anyActivationRequest() {
        new Request(
                "http://localhost:8080/v1/tokens/api/auth/activate",
                HttpMethod.POST,
                new TokenActivationRequest(anyUserPublicId())
        )
    }

    private static String anyUserPublicId() {
        "any user public id"
    }

    private static String anyJwt() {
        "any jwt"
    }
}
