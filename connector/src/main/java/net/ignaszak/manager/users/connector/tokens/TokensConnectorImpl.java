package net.ignaszak.manager.users.connector.tokens;

import net.ignaszak.manager.commons.conf.properties.ManagerProperties;
import net.ignaszak.manager.commons.httpclient.Request;
import net.ignaszak.manager.commons.httpclient.Response;
import net.ignaszak.manager.commons.httpclient.client.HttpClient;
import net.ignaszak.manager.commons.rest.token.TokenActivationRequest;
import net.ignaszak.manager.commons.rest.token.TokenRegistrationRequest;
import org.springframework.stereotype.Component;

import static org.apache.commons.lang3.StringUtils.isAnyBlank;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@Component
class TokensConnectorImpl implements TokensConnectorInterface {

    private static final String TOKEN_REGISTRATION_ENDPOINT = "/v1/tokens/api/auth/register";
    private static final String TOKEN_ACTIVATION_ENDPOINT = "/v1/tokens/api/auth/activate";

    private final HttpClient httpClient;
    private final String gatewayUri;

    TokensConnectorImpl(final HttpClient httpClient, final ManagerProperties managerProperties) throws TokensConnectorException {
        if (isBlank(managerProperties.getServer().getGatewayUri())) {
            throw new TokensConnectorException("Invalid gateway uri!");
        }

        this.httpClient = httpClient;
        this.gatewayUri = managerProperties.getServer().getGatewayUri();
    }

    @Override
    public void register(String jwt, String userPublicId) throws TokensConnectorException {
        if (isAnyBlank(jwt, userPublicId)) {
            throw new TokensConnectorException("Invalid jwt token!");
        }



        var tokenRequest = new TokenRegistrationRequest(userPublicId, jwt);

        var request = new Request.Builder(gatewayUri + TOKEN_REGISTRATION_ENDPOINT)
                .body(tokenRequest)
                .build();

        var response = httpClient.send(request);

        validResponse(request, response, "Could not register jwt token!");
    }

    @Override
    public void activate(String userPublicId) throws TokensConnectorException {
        if (isBlank(userPublicId)) {
            throw new TokensConnectorException("Invalid user public id!");
        }

        var tokenRequest = new TokenActivationRequest(userPublicId);

        var request = new Request.Builder(gatewayUri + TOKEN_ACTIVATION_ENDPOINT)
                .body(tokenRequest)
                .build();

        var response = httpClient.send(request);

        validResponse(request, response, "Could not activate jwt token!");
    }

    private void validResponse(Request request, Response response, String message) throws TokensConnectorException {
        if (!NO_CONTENT.equals(response.getStatus())) {
            throw new TokensConnectorException(message + " " + request.getBody());
        }
    }
}
