package net.ignaszak.manager.users.service;

import lombok.AllArgsConstructor;
import net.ignaszak.manager.commons.error.exception.ValidationException;
import net.ignaszak.manager.commons.restcriteria.service.CriteriaService;
import net.ignaszak.manager.commons.restcriteria.to.CriteriaTO;
import net.ignaszak.manager.commons.utils.datetime.IDateTimeProvider;
import net.ignaszak.manager.users.connector.tokens.TokensConnectorInterface;
import net.ignaszak.manager.users.connector.tokens.TokensConnectorException;
import net.ignaszak.manager.users.entity.UserEntity;
import net.ignaszak.manager.users.service.error.exception.UserActivationException;
import net.ignaszak.manager.users.service.error.exception.UserNotFoundException;
import net.ignaszak.manager.users.service.mapper.UserMapper;
import net.ignaszak.manager.users.service.validator.UserValidator;
import net.ignaszak.manager.users.servicecontract.UserServiceInterface;
import net.ignaszak.manager.users.repository.user.UserRepository;
import net.ignaszak.manager.users.to.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.Period;
import java.util.*;

import static java.util.Collections.unmodifiableSet;
import static java.util.stream.Collectors.toCollection;
import static net.ignaszak.manager.users.service.error.UserError.*;

@Service
@AllArgsConstructor
class UserServiceImpl implements UserServiceInterface {

    private static final int ACTIVATION_CODE_EXPIRATION_MINUTES = 15;
    private static final Random RANDOM = new Random();

    private final UserRepository userRepository;
    private final UserValidator userValidator;
    private final PasswordEncoder passwordEncoder;
    private final UserMapper userMapper;
    private final TokensConnectorInterface tokensConnector;
    private final CriteriaService criteriaService;
    private final IDateTimeProvider dateTimeProvider;

    @Override
    public UserTO getUserByEmail(final String email) {
        userValidator.validEmail(email);

        return userRepository.findUserEntityByEmail(email)
                .map(userMapper::userEntityToUserTO)
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public UserTO getUserByPublicId(final String publicId) {
        userValidator.validPublicId(publicId);

        return userRepository.findUserEntityByPublicId(publicId)
            .filter(UserEntity::isActive)
            .map(userMapper::userEntityToUserTO)
            .orElseThrow(UserNotFoundException::new);
    }

    @Override
    @Transactional
    public UserTO register(final UserRegistrationTO userRegistrationTO) {
        userValidator.validRegistration(userRegistrationTO);

        final var userEntity = getUserEntity(userRegistrationTO);

        setNewActivationCode(userEntity);

        userRepository.save(userEntity);

        return userMapper.userEntityToUserTO(userEntity);
    }

    @Override
    @Transactional
    public UserTO setActivationCode(String publicId) {
        userValidator.validPublicId(publicId);

        final var userEntity = userRepository.findUserEntityByPublicId(publicId)
                .orElseThrow(UserNotFoundException::new);

        if (userEntity.isActive()) {
            throw new ValidationException(USR_ACTIVE);
        }

        setNewActivationCode(userEntity);

        return userMapper.userEntityToUserTO(userEntity);
    }

    @Override
    @Transactional
    public void activate(String publicId, String code) {
        userValidator.validActivationCode(code);

        final var userEntity = userRepository.findUserEntityByPublicIdAndActivationCode(publicId, code)
                .orElseThrow(() -> new ValidationException(USR_INVALID_ACTIVATION_CODE));

        if (dateTimeProvider.getNow().isAfter(userEntity.getActivationCodeExpiration())) {
            throw new ValidationException(USR_INVALID_ACTIVATION_CODE);
        }

        try {
            tokensConnector.activate(publicId);

            userEntity.setActive(true);
            userEntity.setActivationCode(null);
        } catch (TokensConnectorException e) {
            throw new UserActivationException(e);
        }
    }

    @Override
    @Transactional
    public String createResetPasswordToken(String email) {
        userValidator.validEmail(email);

        final var userEntity = userRepository.findUserEntityByEmail(email)
                .orElseThrow(UserNotFoundException::new);

        if (!userEntity.isActive()) {
            throw new UserActivationException();
        }

        final var token = UUID.randomUUID().toString();

        userEntity.setResetPasswordToken(token);
        userEntity.setResetPasswordTokenExpiration(getExpirationLocalDateTime());

        return token;
    }

    @Override
    @Transactional
    public void resetPassword(UserResetPasswordTO userResetPasswordTO) {
        userValidator.validResetPassword(userResetPasswordTO);

        final var userEntity = userRepository.findUserEntityByResetPasswordToken(userResetPasswordTO.token())
                .orElseThrow(UserNotFoundException::new);

        if (dateTimeProvider.getNow().isAfter(userEntity.getResetPasswordTokenExpiration())) {
            throw new ValidationException(USR_INVALID_RESET_PASSWORD_TOKEN);
        }

        final var password = passwordEncoder.encode(userResetPasswordTO.password());

        userEntity.setPassword(password);
        userEntity.setResetPasswordToken(null);
        userEntity.setResetPasswordTokenExpiration(null);
    }

    @Override
    @Transactional
    public UserTO update(final UserUpdateTO userUpdateTO) {
        userValidator.validUpdate(userUpdateTO);

        final var userEntity = userRepository.findUserEntityByPublicId(userUpdateTO.publicId())
                .orElseThrow(UserNotFoundException::new);

        if (!passwordEncoder.matches(userUpdateTO.oldPassword(), userEntity.getPassword())) {
            throw new ValidationException(USR_INVALID_PASSWORD);
        }

        userEntity.setPassword(passwordEncoder.encode(userUpdateTO.newPassword()));

        return userMapper.userEntityToUserTO(userEntity);
    }

    @Override
    public Set<UserMinimumTO> getUserSet(final CriteriaTO<UserFilterTO> criteriaTO) {
        final var criteriaQuery = criteriaService.buildCriteriaQuery(criteriaTO);
        final var users = userRepository.findUserDTOSet(criteriaQuery)
                .stream()
                .map(userMapper::userDTOToUserMinimumTO)
                .collect(toCollection(LinkedHashSet::new));

        return unmodifiableSet(users);
    }

    @Override
    public long countUserSet(final CriteriaTO<UserFilterTO> criteriaTO) {
        final var criteriaQuery = criteriaService.buildCriteriaQuery(criteriaTO);
        return userRepository.countUserDTOSet(criteriaQuery);
    }

    @Override
    @Transactional
    public void deleteInactiveUsersByPeriod(final Period period) {
        userRepository.deleteInactiveUsersByPeriod(period);
    }

    private UserEntity getUserEntity(final UserRegistrationTO userRegistrationTO) {
        final var password = passwordEncoder.encode(userRegistrationTO.password());

        return new UserEntity(userRegistrationTO.email(), password, dateTimeProvider.getNow());
    }

    private void setNewActivationCode(UserEntity userEntity) {
        userEntity.setActivationCode(getRandomCode());
        userEntity.setActivationCodeExpiration(getExpirationLocalDateTime());
    }

    private String getRandomCode() {
        var number = RANDOM.nextInt(999999);

        return String.format("%06d", number);
    }

    private LocalDateTime getExpirationLocalDateTime() {
        return dateTimeProvider.getNow().plusMinutes(ACTIVATION_CODE_EXPIRATION_MINUTES);
    }
}
