package net.ignaszak.manager.users;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import net.ignaszak.manager.commons.email.config.SpringEmailConfig;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;

import static net.ignaszak.manager.commons.openapi.description.AppDescription.CRITERIA_DESCRIPTION;

@SpringBootApplication
@EnableEurekaClient
@EnableRabbit
@EnableScheduling
@Import(SpringEmailConfig.class)
@OpenAPIDefinition(info = @Info(title = "Users API", version = "1.0", description = CRITERIA_DESCRIPTION))
public class UsersApplication {

    public static void main(String[] args) {
        SpringApplication.run(UsersApplication.class, args);
    }
}
