package net.ignaszak.manager.users.security.config;

import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.headers.Header;
import io.swagger.v3.oas.models.media.*;
import io.swagger.v3.oas.models.parameters.RequestBody;
import io.swagger.v3.oas.models.responses.ApiResponse;
import io.swagger.v3.oas.models.responses.ApiResponses;
import lombok.AllArgsConstructor;
import net.ignaszak.manager.commons.conf.properties.ManagerProperties;
import net.ignaszak.manager.commons.rest.response.DataResponse;
import net.ignaszak.manager.commons.rest.response.ErrorResponse;
import net.ignaszak.manager.commons.spring.config.CommonOpenApiConfig;
import net.ignaszak.manager.users.rest.response.UserResponse;
import org.springdoc.core.customizers.OpenApiCustomiser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;

import java.util.List;

import static java.lang.String.valueOf;
import static javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static net.ignaszak.manager.commons.openapi.OpenApiUtils.*;
import static net.ignaszak.manager.commons.spring.security.SecurityConstants.AUTH_LOGIN_URL;
import static net.ignaszak.manager.users.openapi.Description.USER_FILTER_DESCRIPTION;

@Configuration
@AllArgsConstructor
@Import(CommonOpenApiConfig.class)
class OpenApiConfig {

    private static final String STRING = "string";

    private final ManagerProperties managerProperties;

    @Bean
    @Lazy(false)
    OpenApiCustomiser endpointCustomizer() {
        return openApi -> openApi
                    .getPaths()
                    .addPathItem(managerProperties.getServer().getPathPrefix() + AUTH_LOGIN_URL, getUserLoginPath())
                    .addPathItem(managerProperties.getServer().getPathPrefix(), getUsersPath());
    }

    private PathItem getUsersPath() {
        var operation = new Operation();
        operation.responses(getOKListResponse(getUserResponse()));
        operation.setSummary("Get users list");
        operation.addTagsItem("User");
        operation.parameters(List.of(
            getPageParameter(),
            getFilterParameter(USER_FILTER_DESCRIPTION)
        ));

        return new PathItem().get(operation);
    }

    private PathItem getUserLoginPath() {
        var operation = new Operation();

        ApiResponses apiResponses = new ApiResponses();
        apiResponses.addApiResponse(valueOf(HttpStatus.OK.value()), getOKResponse());
        apiResponses.addApiResponse(valueOf(HttpStatus.UNAUTHORIZED.value()), getUnauthorizedResponse());

        operation.responses(apiResponses);
        operation.requestBody(getLoginRequest());
        operation.setSummary("Login user");
        operation.addTagsItem("User auth");

        return new PathItem().post(operation);
    }

    private ApiResponse getOKResponse() {
        var okHeader = new Header()
                .description("User jwt")
                .schema(new StringSchema());

        var okSchema = new Schema<DataResponse<UserResponse>>();
        okSchema.setDefault(new DataResponse<>(getUserResponse()));

        var okMediaType = new MediaType().schema(okSchema);

        var okContent = new Content().addMediaType(APPLICATION_JSON, okMediaType);

        return new ApiResponse()
                .description(HttpStatus.OK.getReasonPhrase())
                .addHeaderObject("Authorization", okHeader)
                .content(okContent);
    }

    private UserResponse getUserResponse() {
        return new UserResponse(STRING, STRING);
    }

    private ApiResponse getUnauthorizedResponse() {
        var okSchema = new Schema<ErrorResponse>();
        okSchema.setDefault(ErrorResponse.of(STRING, STRING));

        var okMediaType = new MediaType().schema(okSchema);

        var okContent = new Content().addMediaType(APPLICATION_JSON, okMediaType);

        return new ApiResponse()
                .description(HttpStatus.UNAUTHORIZED.getReasonPhrase())
                .content(okContent);
    }

    private RequestBody getLoginRequest() {
        var requestSchema = new ObjectSchema()
                .addProperties("username", new StringSchema().description("User email"))
                .addProperties("password", new StringSchema());

        var requestBodyContentMediaType = new MediaType().schema(requestSchema);

        var requestBodyContent = new Content().addMediaType(APPLICATION_FORM_URLENCODED, requestBodyContentMediaType);

        return new RequestBody()
                .content(requestBodyContent);
    }
}
