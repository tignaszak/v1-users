package net.ignaszak.manager.users.rest.request;

import io.swagger.v3.oas.annotations.media.Schema;

import static net.ignaszak.manager.users.rest.openapi.ApiConstants.*;

@Schema(title = "User registration request")
public record UserRegistrationRequest(
        @Schema(description = EMAIL_ADDRESS, required = true) String email,
        @Schema(description = PASSWORD, required = true) String password,
        @Schema(description = PASSWORD_REPEATED, required = true) String passwordRepeated
) {}
