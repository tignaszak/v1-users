package net.ignaszak.manager.users.repository.user;

import net.ignaszak.manager.users.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity, Long>, UserRepositorySupport {

    Optional<UserEntity> findUserEntityByEmail(String email);
    Optional<UserEntity> findUserEntityByPublicId(String publicId);
    Optional<UserEntity> findUserEntityByPublicIdAndActivationCode(String publicId, String code);
    Optional<UserEntity> findUserEntityByResetPasswordToken(String resetPasswordToken);
}
