package net.ignaszak.manager.users.integration

import net.ignaszak.manager.users.servicecontract.UserServiceInterface
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource

import java.time.LocalDateTime
import java.time.Period

class InactiveUsersSchedulerIntegrationSpec extends IntegrationSpecification {

    @Autowired
    private UserServiceInterface userService

    @DynamicPropertySource
    private static void postgresqlProperties(DynamicPropertyRegistry registry) {
        buildConnection(registry)
    }

    def "remove inactive user automatically"() {
        given:
        def user1 = userBuilder.new().setIsActive(false)
        def user2 = userBuilder.new().setIsActive(false).setCreationDate(LocalDateTime.now().minusDays(2))
        insert(user1)
        insert(user2)
        def period = Period.ofDays(1)

        when:
        userService.deleteInactiveUsersByPeriod(period)

        then:
        def projectStmt = CONNECTION.prepareStatement("SELECT count(*) FROM user_account WHERE public_id = ?")
        projectStmt.setString(1, user2.publicId)
        def projectResultSet = projectStmt.executeQuery()
        projectResultSet.next()
        projectResultSet.getInt(1) == 0

        and:
        def projectStmt1 = CONNECTION.prepareStatement("SELECT count(*) FROM user_account WHERE public_id = ?")
        projectStmt1.setString(1, user1.publicId)
        def projectResultSet1 = projectStmt1.executeQuery()
        projectResultSet1.next()
        projectResultSet1.getInt(1) == 1
    }
}
