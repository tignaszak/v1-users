package net.ignaszak.manager.users

import net.ignaszak.manager.commons.spring.config.CommonWebMvcConfig
import net.ignaszak.manager.commons.spring.handler.repository.RequestMappingRepositorySingleton
import net.ignaszak.manager.commons.spring.security.matcher.UriMatcherImpl
import net.ignaszak.manager.users.config.AppConfig
import net.ignaszak.manager.users.connector.tokens.TokensConnectorInterface
import net.ignaszak.manager.commons.spring.security.mapper.UserSecurityMapper
import net.ignaszak.manager.users.repository.user.UserRepository
import net.ignaszak.manager.users.security.config.SecurityConfig
import net.ignaszak.manager.users.security.mapper.UserPrincipalMapper
import org.spockframework.spring.SpringBean
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary
import org.springframework.context.annotation.Profile
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.context.event.EventListener
import org.springframework.security.core.userdetails.UserDetailsService

import static net.ignaszak.manager.commons.test.helper.JwtHelper.anyJwtUserTO
import static net.ignaszak.manager.commons.test.helper.UserHelper.anyUserPrincipalTO

class ControllerSpecification extends net.ignaszak.manager.commons.test.specification.ControllerSpecification {

    @SpringBean(name = "customUserDetailsService")
    protected UserDetailsService userDetailsService = Mock()

    @SpringBean
    protected RabbitTemplate rabbitTemplateMock = Mock()

    @SpringBean
    protected UserSecurityMapper userSecurityMapperMock = Mock()

    @SpringBean
    protected UserPrincipalMapper securityMapperMock = Mock()

    @SpringBean
    protected TokensConnectorInterface tokensConnectorMock = Mock()

    @SpringBean
    protected UserRepository userRepositoryMock = Mock()

    @SpringBean
    protected AppConfig appConfigMock = Mock()

    def setup() {
        userSecurityMapperMock.jwtUserTOToUserPrincipalTO(anyJwtUserTO()) >> anyUserPrincipalTO()
    }

    @TestConfiguration
    @Profile("test")
    static class Conf {
        @Bean("securityConfig")
        @Primary
        SecurityConfig securityConfig() {
            return new SecurityConfig(
                    userDetailsService,
                    jwtServiceMock,
                    userSecurityMapperMock,
                    securityMapperMock,
                    new UriMatcherImpl(RequestMappingRepositorySingleton.INSTANCE),
                    tokensConnectorMock,
                    managerProperties,
                    userRepositoryMock,
                    appConfigMock
            )
        }

        @EventListener
        void handleContextRefresh(ContextRefreshedEvent event) {
            CommonWebMvcConfig.handleRequestMappings(event, managerProperties)
        }
    }
}

