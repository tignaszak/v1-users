package net.ignaszak.manager.users.security.config;

import net.ignaszak.manager.commons.httpclient.client.HttpClient;
import net.ignaszak.manager.commons.httpclient.client.OAuth2HttpClient;
import net.ignaszak.manager.commons.httpclient.client.provider.Oauth2ProviderBuilder;
import net.ignaszak.manager.commons.spring.config.CommonOAuth2ClientConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.client.RestTemplate;

@Configuration
@Import(CommonOAuth2ClientConfig.class)
public class OAuth2HttpClientConfig {

    @Bean
    public HttpClient oAuth2TokensHttpClient(Oauth2ProviderBuilder oauth2ProviderBuilder) {
        var restTemplate = new RestTemplate();
        var oAuth2Provider = oauth2ProviderBuilder
                .withClientRegistrationId("tokens")
                .build();

        return new OAuth2HttpClient(restTemplate, oAuth2Provider);
    }
}
