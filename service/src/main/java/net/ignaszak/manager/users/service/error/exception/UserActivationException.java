package net.ignaszak.manager.users.service.error.exception;

import net.ignaszak.manager.commons.error.exception.ErrorException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import static net.ignaszak.manager.users.service.error.UserError.USR_ACTIVATION;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UserActivationException extends ErrorException {
    public UserActivationException() {
        super(USR_ACTIVATION);
    }

    public UserActivationException(Throwable cause) {
        super(USR_ACTIVATION, cause);
    }
}
