package net.ignaszak.manager.users.security.service.impl

import net.ignaszak.manager.commons.error.exception.ValidationException
import net.ignaszak.manager.users.repository.user.UserRepository
import net.ignaszak.manager.users.security.model.UserSecurityModel
import net.ignaszak.manager.users.security.service.UserSecurityService
import net.ignaszak.manager.users.service.error.UserError
import net.ignaszak.manager.users.service.error.exception.UserNotFoundException
import net.ignaszak.manager.users.service.validator.UserValidator
import spock.lang.*

import static net.ignaszak.manager.users.TestConstants.INVALID_EMAIL
import static net.ignaszak.manager.users.TestConstants.VALID_EMAIL
import static net.ignaszak.manager.users.TestConstants.VALID_PASSWORD
import static net.ignaszak.manager.users.TestHelper.anyUserEntity
import static net.ignaszak.manager.users.service.error.UserError.USR_INVALID_EMAIL
import static org.junit.platform.commons.util.StringUtils.isNotBlank

class UserSecurityServiceImplSpec extends Specification {

    private UserRepository userRepository = Mock()
    private UserValidator userValidator = Mock()

    private UserSecurityService subject

    def setup() {
        subject = new UserSecurityServiceImpl(userRepository, userValidator)
    }

    def "return 'UserSecurityModel' when email is correct and user exists"() {
        given:
        1 * userRepository.findUserEntityByEmail(VALID_EMAIL) >> Optional.of(anyUserEntity())

        when:
        UserSecurityModel result = subject.getUserSecurityModelByEmail(VALID_EMAIL)

        then:
        result.password() == VALID_PASSWORD
        result.userPrincipalModel().email() == VALID_EMAIL
        isNotBlank(result.userPrincipalModel().publicId())
    }

    def "throw exception when email is invalid"() {
        given:
        1 * userValidator.validEmail(INVALID_EMAIL) >> {throw new ValidationException(USR_INVALID_EMAIL)}

        when:
        subject.getUserSecurityModelByEmail(INVALID_EMAIL)

        then:
        def e = thrown ValidationException
        e.getMessage() == USR_INVALID_EMAIL.toString()
        0 * userRepository.findUserEntityByEmail(any())
    }

    def "throw exception when user does not exist"() {
        given:
        1 * userRepository.findUserEntityByEmail(VALID_EMAIL) >> Optional.empty()

        when:
        subject.getUserSecurityModelByEmail(VALID_EMAIL)

        then:
        def e = thrown UserNotFoundException
        e.getMessage() == UserError.USR_USER_NOT_FOUND.toString()
    }
}