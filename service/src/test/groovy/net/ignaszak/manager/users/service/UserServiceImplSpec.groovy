package net.ignaszak.manager.users.service

import net.ignaszak.manager.commons.error.exception.ValidationException
import net.ignaszak.manager.commons.restcriteria.service.CriteriaService
import net.ignaszak.manager.commons.utils.datetime.IDateTimeProvider
import net.ignaszak.manager.users.connector.tokens.TokensConnectorInterface
import net.ignaszak.manager.users.connector.tokens.TokensConnectorException
import net.ignaszak.manager.users.dto.user.UserDTO
import net.ignaszak.manager.users.entity.UserEntity
import net.ignaszak.manager.users.repository.user.UserRepository
import net.ignaszak.manager.users.service.error.UserError
import net.ignaszak.manager.users.service.error.exception.UserActivationException
import net.ignaszak.manager.users.service.error.exception.UserNotFoundException
import net.ignaszak.manager.users.service.mapper.UserMapper
import net.ignaszak.manager.users.service.validator.UserValidator
import net.ignaszak.manager.users.servicecontract.UserServiceInterface
import net.ignaszak.manager.users.to.UserRegistrationTO
import net.ignaszak.manager.users.to.UserTO
import net.ignaszak.manager.users.to.UserUpdateTO
import org.springframework.security.crypto.password.PasswordEncoder
import spock.lang.*

import java.time.LocalDateTime

import static net.ignaszak.manager.users.TestHelper.ACTIVATION_CODE
import static net.ignaszak.manager.users.TestHelper.ANY_PUBLIC_ID
import static net.ignaszak.manager.users.TestHelper.NEW_ENCODED_PASSWORD
import static net.ignaszak.manager.users.TestHelper.NEW_VALID_PASSWORD
import static net.ignaszak.manager.users.TestHelper.RESET_PASSWORD_TOKEN
import static net.ignaszak.manager.users.TestHelper.VALID_EMAIL
import static net.ignaszak.manager.users.TestHelper.ENCODED_PASSWORD
import static net.ignaszak.manager.users.TestHelper.INVALID_EMAIL
import static net.ignaszak.manager.users.TestHelper.VALID_PASSWORD
import static net.ignaszak.manager.users.TestHelper.anyUserCriteriaTO
import static net.ignaszak.manager.users.TestHelper.anyUserEntity
import static net.ignaszak.manager.users.TestHelper.anyUserCriteriaQuery
import static net.ignaszak.manager.users.TestHelper.anyUserRegistrationTO
import static net.ignaszak.manager.users.TestHelper.anyUserResetPasswordTO
import static net.ignaszak.manager.users.TestHelper.anyUserTO
import static net.ignaszak.manager.users.service.error.UserError.USR_ACTIVE
import static net.ignaszak.manager.users.service.error.UserError.USR_INVALID_ACTIVATION_CODE
import static net.ignaszak.manager.users.service.error.UserError.USR_INVALID_PASSWORD
import static net.ignaszak.manager.users.service.error.UserError.USR_INVALID_RESET_PASSWORD_TOKEN
import static net.ignaszak.manager.users.service.error.UserError.USR_USER_NOT_FOUND
import static org.apache.commons.lang3.StringUtils.isEmpty
import static org.junit.platform.commons.util.StringUtils.isNotBlank

class UserServiceImplSpec extends Specification {

    private static final UserEntity ANY_USER_ENTITY = anyUserEntity()
    private static final UserRegistrationTO ANY_USER_REGISTRATION_TO = anyUserRegistrationTO()

    private PasswordEncoder passwordEncoderMock = Mock()
    private UserRepository userRepositoryMock = Mock()
    private UserValidator userValidatorMock = Mock()
    private UserMapper userMapperMock = Mock()
    private TokensConnectorInterface tokensConnectorMock = Mock()
    private CriteriaService criteriaServiceMock = Mock()

    private dateTimeProvider = new IDateTimeProvider() {
        @Override
        LocalDateTime getNow() {
            return LocalDateTime.now()
        }
    }

    private UserServiceInterface subject

    def setup() {
        subject = new UserServiceImpl(
                userRepositoryMock,
                userValidatorMock,
                passwordEncoderMock,
                userMapperMock,
                tokensConnectorMock,
                criteriaServiceMock,
                dateTimeProvider
        )
    }

    def "return 'UserTO' when email is correct and user exists"() {
        given:
        1 * userRepositoryMock.findUserEntityByEmail(VALID_EMAIL) >> Optional.of(ANY_USER_ENTITY)
        1 * userMapperMock.userEntityToUserTO(ANY_USER_ENTITY) >> anyUserTO()

        when:
        UserTO result = subject.getUserByEmail(VALID_EMAIL)

        then:
        result.email() == VALID_EMAIL
        isNotBlank result.publicId()
    }

    def "throw exception when email is invalid"() {
        given:
        1 * userValidatorMock.validEmail(INVALID_EMAIL) >> {throw new ValidationException(UserError.USR_INVALID_EMAIL)}

        when:
        subject.getUserByEmail(INVALID_EMAIL)

        then:
        def e = thrown ValidationException
        e.getMessage() == UserError.USR_INVALID_EMAIL.toString()
        0 * userRepositoryMock.findUserEntityByEmail(any())
    }

    def "throw exception when user does not exist while getting by email"() {
        given:
        1 * userRepositoryMock.findUserEntityByEmail(VALID_EMAIL) >> Optional.empty()

        when:
        subject.getUserByEmail(VALID_EMAIL)

        then:
        def e = thrown UserNotFoundException
        e.getMessage() == USR_USER_NOT_FOUND.toString()
    }

    def "return 'UserTO' with activation code when registration is successful"() {
        given:
        1 * userValidatorMock.validRegistration(ANY_USER_REGISTRATION_TO)
        1 * passwordEncoderMock.encode(VALID_PASSWORD) >> ENCODED_PASSWORD

        when:
        UserTO result = subject.register(ANY_USER_REGISTRATION_TO)

        then:
        1 * userMapperMock.userEntityToUserTO({
            it.getActivationCode().length() == 6
        }) >> anyUserTO()
        result.email() == VALID_EMAIL
        isNotBlank result.publicId()
        1 * userRepositoryMock.save({ UserEntity userEntity ->
            userEntity.getEmail() == VALID_EMAIL
            userEntity.getPassword() == ENCODED_PASSWORD
            isNotBlank userEntity.getPublicId()
            userEntity.activationCodeExpiration.isAfter(LocalDateTime.now().plusMinutes(14))
        })
    }

    def "throw an exception when registration data are incorrect"() {
        given:
        def model = ANY_USER_REGISTRATION_TO
        1 * userValidatorMock.validRegistration(model) >> {throw new ValidationException(UserError.USR_USER_EXISTS)}

        when:
        subject.register(model)

        then:
        def e = thrown ValidationException
        e.getMessage() == UserError.USR_USER_EXISTS.toString()
        0 * userRepositoryMock.save(any())
        0 * passwordEncoderMock.encode(any())
    }

    def "return 'UserTO' when public id is correct and user exists"() {
        given:
        1 * userRepositoryMock.findUserEntityByPublicId(ANY_PUBLIC_ID) >> Optional.of(ANY_USER_ENTITY)
        1 * userMapperMock.userEntityToUserTO(ANY_USER_ENTITY) >> anyUserTO()

        when:
        UserTO result = subject.getUserByPublicId(ANY_PUBLIC_ID)

        then:
        result.publicId() == ANY_PUBLIC_ID
        isNotBlank result.email()
    }

    def "throw an exception while getting inactive user by public id" () {
        given:
        def inactiveUser = anyUserEntity()
        inactiveUser.active = false
        1 * userRepositoryMock.findUserEntityByPublicId(ANY_PUBLIC_ID) >> Optional.of(inactiveUser)

        when:
        subject.getUserByPublicId(ANY_PUBLIC_ID)

        then:
        def e = thrown UserNotFoundException
        e.getMessage() == USR_USER_NOT_FOUND.toString()

        and:
        0 * userMapperMock.userEntityToUserTO(_)
    }

    def "throw exception when public id is invalid"() {
        given:
        1 * userValidatorMock.validPublicId("") >> {throw new ValidationException(UserError.USR_INVALID_PUBLIC_ID)}

        when:
        subject.getUserByPublicId("")

        then:
        def e = thrown ValidationException
        e.getMessage() == UserError.USR_INVALID_PUBLIC_ID.toString()
        0 * userRepositoryMock.findUserEntityByPublicId(any())
    }

    def "throw exception when user does not exist while getting by public id"() {
        given:
        1 * userRepositoryMock.findUserEntityByEmail(VALID_EMAIL) >> Optional.empty()

        when:
        subject.getUserByEmail(VALID_EMAIL)

        then:
        def e = thrown UserNotFoundException
        e.getMessage() == USR_USER_NOT_FOUND.toString()
    }

    def "successful activate user when activation code is valid, not expired, exists in db and tokens activation succeed" () {
        given:
        def anyUserEntity = anyUserEntity()
        anyUserEntity.setActive(false)
        anyUserEntity.setActivationCode(ACTIVATION_CODE)
        anyUserEntity.setActivationCodeExpiration(LocalDateTime.now().plusMinutes(10))
        1 * userRepositoryMock.findUserEntityByPublicIdAndActivationCode(ANY_PUBLIC_ID, ACTIVATION_CODE) >> Optional.of(anyUserEntity)

        when:
        subject.activate(ANY_PUBLIC_ID, ACTIVATION_CODE)

        then:
        1 * userValidatorMock.validActivationCode(ACTIVATION_CODE)
        1 * tokensConnectorMock.activate(ANY_PUBLIC_ID)
        anyUserEntity.isActive()
        isEmpty anyUserEntity.activationCode
    }

    def "fail to activate user when activation code is valid but expired" () {
        given:
        def anyUserEntity = anyUserEntity()
        anyUserEntity.setActive(false)
        anyUserEntity.setActivationCode(ACTIVATION_CODE)
        anyUserEntity.setActivationCodeExpiration(LocalDateTime.now().minusMinutes(10))
        1 * userRepositoryMock.findUserEntityByPublicIdAndActivationCode(ANY_PUBLIC_ID, ACTIVATION_CODE) >> Optional.of(anyUserEntity)

        when:
        subject.activate(ANY_PUBLIC_ID, ACTIVATION_CODE)

        then:
        1 * userValidatorMock.validActivationCode(ACTIVATION_CODE)
        def e = thrown ValidationException
        e.getMessage() == USR_INVALID_ACTIVATION_CODE.toString()
    }

    @SuppressWarnings("all")
    def "fail to activate user when activation code is valid, exists in db and tokens activation fails" () {
        given:
        def anyUserEntity = anyUserEntity()
        anyUserEntity.setActive(false)
        anyUserEntity.setActivationCode(ACTIVATION_CODE)
        anyUserEntity.setActivationCodeExpiration(LocalDateTime.now().plusMinutes(10))
        1 * userRepositoryMock.findUserEntityByPublicIdAndActivationCode(ANY_PUBLIC_ID, ACTIVATION_CODE) >> Optional.of(anyUserEntity)
        1 * tokensConnectorMock.activate(ANY_PUBLIC_ID) >> {throw new TokensConnectorException("")}

        when:
        subject.activate(ANY_PUBLIC_ID, ACTIVATION_CODE)

        then:
        1 * userValidatorMock.validActivationCode(ACTIVATION_CODE)
        def e = thrown UserActivationException
        e.getMessage() == UserError.USR_ACTIVATION.toString()
    }

    def "fail to activate user when activation code is invalid" () {
        given:
        1 * userValidatorMock.validActivationCode(ACTIVATION_CODE) >> {throw new ValidationException(USR_INVALID_ACTIVATION_CODE)}

        when:
        subject.activate(ANY_PUBLIC_ID, ACTIVATION_CODE)

        then:
        0 * userRepositoryMock.findUserEntityByPublicIdAndActivationCode(ANY_PUBLIC_ID, ACTIVATION_CODE)
        0 * tokensConnectorMock.activate(ANY_PUBLIC_ID)
        def e = thrown ValidationException
        e.getMessage() == USR_INVALID_ACTIVATION_CODE.toString()
    }

    def "fail to activate user when activation code is valid and does not match ones in db" () {
        given:
        def anyUserEntity = anyUserEntity()
        anyUserEntity.setActive(false)
        anyUserEntity.setActivationCode(ACTIVATION_CODE)
        1 * userRepositoryMock.findUserEntityByPublicIdAndActivationCode(ANY_PUBLIC_ID, ACTIVATION_CODE) >> Optional.empty()

        when:
        subject.activate(ANY_PUBLIC_ID, ACTIVATION_CODE)

        then:
        1 * userValidatorMock.validActivationCode(ACTIVATION_CODE)
        0 * tokensConnectorMock.activate(ANY_PUBLIC_ID)
        def e = thrown ValidationException
        e.getMessage() == USR_INVALID_ACTIVATION_CODE.toString()
    }

    def "generate reset password token"() {
        given:
        def userEntity = anyUserEntity()
        userEntity.setActive(true)
        1 * userRepositoryMock.findUserEntityByEmail(anyUserTO().email()) >> Optional.of(userEntity)

        when:
        def token = subject.createResetPasswordToken(anyUserTO().email())

        then:
        1 * userValidatorMock.validEmail(anyUserTO().email())
        isNotBlank token
        userEntity.resetPasswordToken == token
        userEntity.resetPasswordTokenExpiration.isAfter(LocalDateTime.now().plusMinutes(14))
    }

    def "throw exception when user is inactive while generating reset password token"() {
        given:
        def userEntity = anyUserEntity()
        userEntity.active = false
        1 * userRepositoryMock.findUserEntityByEmail(anyUserTO().email()) >> Optional.of(userEntity)

        when:
        subject.createResetPasswordToken(anyUserTO().email())

        then:
        1 * userValidatorMock.validEmail(anyUserTO().email())
        def e = thrown UserActivationException
        e.getMessage() == UserError.USR_ACTIVATION.toString()
    }

    def "throw exception when email is incorrect while generating reset password token"() {
        given:
        1 * userValidatorMock.validEmail(anyUserTO().email()) >> {throw new ValidationException(UserError.USR_INVALID_EMAIL)}

        when:
        subject.createResetPasswordToken(anyUserTO().email())

        then:
        0 * userRepositoryMock.findUserEntityByEmail(any())
        def e = thrown ValidationException
        e.getMessage() == UserError.USR_INVALID_EMAIL.toString()
    }

    def "throw exception when user is not found while generating reset password token"() {
        given:
        1 * userRepositoryMock.findUserEntityByEmail(anyUserTO().email()) >> Optional.empty()

        when:
        subject.createResetPasswordToken(anyUserTO().email())

        then:
        1 * userValidatorMock.validEmail(anyUserTO().email())
        def e = thrown UserNotFoundException
        e.getMessage() == USR_USER_NOT_FOUND.toString()
    }

    def "set new activation code"() {
        given:
        def userEntity = anyUserEntity()
        userEntity.active = false
        1 * userRepositoryMock.findUserEntityByPublicId(anyUserTO().publicId()) >> Optional.of(userEntity)

        when:
        subject.setActivationCode(anyUserTO().publicId())

        then:
        1 * userValidatorMock.validPublicId(anyUserTO().publicId())
        1 * userMapperMock.userEntityToUserTO(userEntity)
        userEntity.getActivationCode().length() == 6
        userEntity.activationCodeExpiration.isAfter(LocalDateTime.now().plusMinutes(14))
    }

    def "throw exception when public id is incorrect while setting activation code"() {
        given:
        1 * userValidatorMock.validPublicId(anyUserTO().publicId()) >> {throw new ValidationException(UserError.USR_INVALID_PUBLIC_ID)}

        when:
        subject.setActivationCode(anyUserTO().publicId())

        then:
        0 * userRepositoryMock.findUserEntityByPublicId(any())
        0 * userMapperMock.userEntityToUserTO(any())
        def e = thrown ValidationException
        e.getMessage() == UserError.USR_INVALID_PUBLIC_ID.toString()
    }

    def "throw exception when user not exists while setting activation code"() {
        given:
        1 * userRepositoryMock.findUserEntityByPublicId(anyUserTO().publicId()) >> Optional.empty()

        when:
        subject.setActivationCode(anyUserTO().publicId())

        then:
        1 * userValidatorMock.validPublicId(anyUserTO().publicId())
        0 * userMapperMock.userEntityToUserTO(any())
        def e = thrown UserNotFoundException
        e.getMessage() == USR_USER_NOT_FOUND.toString()
    }

    def "throw exception when user is active while setting activation code"() {
        given:
        def userEntity = anyUserEntity()
        userEntity.setActive(true)
        1 * userRepositoryMock.findUserEntityByPublicId(anyUserTO().publicId()) >> Optional.of(userEntity)

        when:
        subject.setActivationCode(anyUserTO().publicId())

        then:
        1 * userValidatorMock.validPublicId(anyUserTO().publicId())
        0 * userMapperMock.userEntityToUserTO(any())
        def e = thrown ValidationException
        e.getMessage() == USR_ACTIVE.toString()
    }

    def "successful reset password"() {
        given:
        def userEntity = anyUserEntity()
        userEntity.setResetPasswordToken(RESET_PASSWORD_TOKEN)
        userEntity.setResetPasswordTokenExpiration(LocalDateTime.now().plusMinutes(10))
        1 * userRepositoryMock.findUserEntityByResetPasswordToken(RESET_PASSWORD_TOKEN) >> Optional.of(userEntity)
        1 * passwordEncoderMock.encode(NEW_VALID_PASSWORD) >> NEW_ENCODED_PASSWORD

        when:
        subject.resetPassword(anyUserResetPasswordTO())

        then:
        1 * userValidatorMock.validResetPassword(anyUserResetPasswordTO())
        userEntity.getPassword() == NEW_ENCODED_PASSWORD
        userEntity.getResetPasswordToken() == null
        userEntity.getResetPasswordTokenExpiration() == null
    }

    def "throw exception when reset password token is expired" () {
        given:
        def userEntity = anyUserEntity()
        userEntity.setResetPasswordToken(RESET_PASSWORD_TOKEN)
        userEntity.setResetPasswordTokenExpiration(LocalDateTime.now().minusMinutes(10))
        1 * userRepositoryMock.findUserEntityByResetPasswordToken(RESET_PASSWORD_TOKEN) >> Optional.of(userEntity)

        when:
        subject.resetPassword(anyUserResetPasswordTO())

        then:
        1 * userValidatorMock.validResetPassword(anyUserResetPasswordTO())
        0 * passwordEncoderMock.encode(any())
        def e = thrown ValidationException
        e.getMessage() == USR_INVALID_RESET_PASSWORD_TOKEN.toString()
    }

    def "throw exception when UserResetPasswordTO is incorrect while resetting password" () {
        given:
        1 * userValidatorMock.validResetPassword(anyUserResetPasswordTO()) >> {throw new ValidationException(USR_INVALID_RESET_PASSWORD_TOKEN)}

        when:
        subject.resetPassword(anyUserResetPasswordTO())

        then:
        0 * userRepositoryMock.findUserEntityByResetPasswordToken(any())
        0 * passwordEncoderMock.encode(any())
        def e = thrown ValidationException
        e.getMessage() == USR_INVALID_RESET_PASSWORD_TOKEN.toString()
    }

    def "throw exception when user is not found by reset password token" () {
        given:
        userRepositoryMock.findUserEntityByResetPasswordToken(RESET_PASSWORD_TOKEN) >> Optional.empty()

        when:
        subject.resetPassword(anyUserResetPasswordTO())

        then:
        1 * userValidatorMock.validResetPassword(anyUserResetPasswordTO())
        0 * passwordEncoderMock.encode(any())
        def e = thrown UserNotFoundException
        e.getMessage() == USR_USER_NOT_FOUND.toString()
    }

    def "successful update user data" () {
        given:
        def userEntity = anyUserEntity()
        1 * passwordEncoderMock.matches(VALID_PASSWORD, ENCODED_PASSWORD) >> true
        1 * passwordEncoderMock.encode(NEW_VALID_PASSWORD) >> NEW_ENCODED_PASSWORD
        1 * userRepositoryMock.findUserEntityByPublicId(ANY_PUBLIC_ID) >> Optional.of(userEntity)
        1 * userMapperMock.userEntityToUserTO(userEntity) >> anyUserTO()

        when:
        subject.update(anyUserUpdateTO())

        then:
        userEntity.getPassword() == NEW_ENCODED_PASSWORD
        1 * userValidatorMock.validUpdate(anyUserUpdateTO())
    }

    def "throw exception when UserUpdateTO is incorrect" () {
        given:
        1 * userValidatorMock.validUpdate(anyUserUpdateTO()) >> {throw new ValidationException(USR_INVALID_PASSWORD)}

        when:
        subject.update(anyUserUpdateTO())

        then:
        def e = thrown ValidationException
        e.getMessage() == USR_INVALID_PASSWORD.toString()
        0 * userRepositoryMock.findUserEntityByPublicId(any())
        0 * passwordEncoderMock.encode(any())
        0 * passwordEncoderMock.matches(any(), any())
        0 * userMapperMock.userEntityToUserTO(any())
    }

    def "throw exception when user is not found while updating user" () {
        given:
        1 * userRepositoryMock.findUserEntityByPublicId(ANY_PUBLIC_ID) >> Optional.empty()

        when:
        subject.update(anyUserUpdateTO())

        then:
        def e = thrown UserNotFoundException
        e.getMessage() == USR_USER_NOT_FOUND.toString()
        1 * userValidatorMock.validUpdate(anyUserUpdateTO())
        0 * passwordEncoderMock.encode(any())
        0 * passwordEncoderMock.matches(any(), any())
        0 * userMapperMock.userEntityToUserTO(any())
    }

    def "throw exception when old password is incorrect while updating user" () {
        given:
        1 * userRepositoryMock.findUserEntityByPublicId(ANY_PUBLIC_ID) >> Optional.of(anyUserEntity())
        1 * passwordEncoderMock.matches(VALID_PASSWORD, ENCODED_PASSWORD) >> false

        when:
        subject.update(anyUserUpdateTO())

        then:
        def e = thrown ValidationException
        e.getMessage() == USR_INVALID_PASSWORD.toString()
        1 * userValidatorMock.validUpdate(anyUserUpdateTO())
        0 * passwordEncoderMock.encode(any())
        0 * userMapperMock.userEntityToUserTO(any())
    }

    def "successful return user list"() {
        given:
        1 * criteriaServiceMock.buildCriteriaQuery(anyUserCriteriaTO()) >> anyUserCriteriaQuery()
        1 * userRepositoryMock.findUserDTOSet(anyUserCriteriaQuery()) >> {
            def set = new LinkedHashSet()
            set.add(new UserDTO("publicId1", "first@email.pl"))
            set.add(new UserDTO("publicId2", "second@email.pl"))
            set.add(new UserDTO("publicId2", "third@email.pl"))
            return set
        }

        when:
        subject.getUserSet(anyUserCriteriaTO())

        then:
        3 * userMapperMock.userDTOToUserMinimumTO(_)
    }

    private static UserUpdateTO anyUserUpdateTO() {
        new UserUpdateTO(ANY_PUBLIC_ID, VALID_PASSWORD, NEW_VALID_PASSWORD, NEW_VALID_PASSWORD)
    }
}