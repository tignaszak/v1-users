package net.ignaszak.manager.users.security.model;

public record UserSecurityModel(String password, UserPrincipalModel userPrincipalModel) {}
