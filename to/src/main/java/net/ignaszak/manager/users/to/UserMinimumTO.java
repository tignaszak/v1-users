package net.ignaszak.manager.users.to;

public record UserMinimumTO(String publicId, String email) {}
