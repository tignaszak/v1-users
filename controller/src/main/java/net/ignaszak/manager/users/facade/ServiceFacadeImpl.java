package net.ignaszak.manager.users.facade;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import net.ignaszak.manager.commons.email.annotation.SimpleEmail;
import net.ignaszak.manager.commons.rest.page.PageRequest;
import net.ignaszak.manager.commons.rest.response.DataResponse;
import net.ignaszak.manager.commons.rest.response.ListResponse;
import net.ignaszak.manager.commons.restcriteria.paging.service.PagingService;
import net.ignaszak.manager.users.config.AppConfig;
import net.ignaszak.manager.users.connector.email.EmailConnectorInterface;
import net.ignaszak.manager.users.mapper.ControllerMapper;
import net.ignaszak.manager.users.mapper.RestCriteriaMapper;
import net.ignaszak.manager.users.rest.filter.UserFilterRequest;
import net.ignaszak.manager.users.rest.request.UserRegistrationRequest;
import net.ignaszak.manager.users.rest.request.UserResetPasswordRequest;
import net.ignaszak.manager.users.rest.request.UserUpdateRequest;
import net.ignaszak.manager.users.rest.response.UserResponse;
import net.ignaszak.manager.users.servicecontract.UserServiceInterface;
import net.ignaszak.manager.users.to.UserFilterTO;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

import java.util.LinkedHashSet;

import static java.util.Collections.unmodifiableSet;
import static java.util.stream.Collectors.toCollection;
import static net.ignaszak.manager.commons.email.pattern.EmailPattern.REGISTRATION;

@Component
@AllArgsConstructor
@Log4j2
@EnableAsync
class ServiceFacadeImpl implements ServiceFacade {

    private final UserServiceInterface userService;
    private final EmailConnectorInterface emailConnector;
    private final ControllerMapper controllerMapper;
    private final RestCriteriaMapper<UserFilterRequest, UserFilterTO> restCriteriaMapper;
    private final PagingService pagingService;
    private final AppConfig appConfig;

    @Override
    @SimpleEmail(REGISTRATION)
    public DataResponse<UserResponse> registerUser(final UserRegistrationRequest userRegistrationRequest) {
        final var userRegistrationTO = controllerMapper.userRegistrationRequestToUserRegistrationTO(userRegistrationRequest);
        final var userTO = userService.register(userRegistrationTO);
        final var userResponse = controllerMapper.userTOToUserResponse(userTO);
        final var registrationData = controllerMapper.userTOToRegistrationData(userTO);

        return new DataResponse<>(userResponse, registrationData);
    }

    @Override
    @SimpleEmail(REGISTRATION)
    public DataResponse<UserResponse> sendActivationCode(final String publicId) {
        final var userTO = userService.setActivationCode(publicId);
        final var userResponse = controllerMapper.userTOToUserResponse(userTO);
        final var registrationData = controllerMapper.userTOToRegistrationData(userTO);

        return new DataResponse<>(userResponse, registrationData);
    }

    @Override
    public void activateUser(final String publicId, final String activationCode) {
        userService.activate(publicId, activationCode);
    }

    @Override
    @Async
    public void sendResetPasswordTokenAsync(final String email) {
        try {
            final var token = userService.createResetPasswordToken(email);
            emailConnector.sendResetPasswordEmail(email, token);
        } catch (Exception e) {
            log.error("Could not send reset password token.", e);
        }
    }

    @Override
    public DataResponse<UserResponse> getUserByPublicId(final String publicId) {
        final var userTO = userService.getUserByPublicId(publicId);
        final var userResponse = controllerMapper.userTOToUserResponse(userTO);

        return new DataResponse<>(userResponse);
    }

    @Override
    public void resetPassword(final String token, final UserResetPasswordRequest userResetPasswordRequest) {
        final var userResetPasswordTO = controllerMapper.tokenAndUserResetPasswordRequestToUserResetPasswordTO(token, userResetPasswordRequest);
        userService.resetPassword(userResetPasswordTO);
    }

    @Override
    public DataResponse<UserResponse> updateUser(final String publicId, final UserUpdateRequest userUpdateRequest) {
        final var userUpdateTO = controllerMapper.publicIdAndUserUpdateRequestToUserUpdateTO(publicId, userUpdateRequest);
        final var userTO = userService.update(userUpdateTO);
        final var userResponse = controllerMapper.userTOToUserResponse(userTO);

        return new DataResponse<>(userResponse);
    }

    @Override
    public ListResponse<UserResponse> getUserList(final PageRequest pageRequest, final UserFilterRequest filter) {
        final var criteriaTO = restCriteriaMapper.pageRequestAndFilterRequestToCriteriaTO(pageRequest, filter);
        final var users = userService.getUserSet(criteriaTO)
                .stream()
                .map(controllerMapper::userMinimumTOToUserResponse)
                .collect(toCollection(LinkedHashSet::new));
        final var pageTO = pagingService.buildPageTO(userService.countUserSet(criteriaTO), criteriaTO.getPageCriteriaTO());
        final var pageResponse = restCriteriaMapper.getCommonRestCriteriaMapper().pageTOToPageResponse(pageTO);

        return new ListResponse<>(pageResponse, unmodifiableSet(users));
    }

    @Override
    @Async
    public void deleteInactiveUsersAsync() {
        try {
            log.info("Init inactive users cleaner.");
            userService.deleteInactiveUsersByPeriod(appConfig.getDeleteInactiveUserPeriod());
        } catch (final Exception e) {
            log.error("Could not execute inactive users cleaner!", e);
        }
    }
}
