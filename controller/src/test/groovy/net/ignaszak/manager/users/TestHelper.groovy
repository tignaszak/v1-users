package net.ignaszak.manager.users

import lombok.experimental.UtilityClass
import net.ignaszak.manager.commons.rest.response.DataResponse
import net.ignaszak.manager.users.entity.UserEntity
import net.ignaszak.manager.users.rest.request.UserActivationRequest
import net.ignaszak.manager.users.rest.request.UserRegistrationRequest
import net.ignaszak.manager.users.rest.request.UserResetPasswordRequest
import net.ignaszak.manager.users.rest.request.UserResetPasswordTokenRequest
import net.ignaszak.manager.users.rest.response.UserResponse
import net.ignaszak.manager.users.to.UserTO
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.ResultMatcher

import java.time.LocalDateTime

import static net.ignaszak.manager.commons.test.TestUtils.assertResponse
import static net.ignaszak.manager.commons.test.helper.UserHelper.ANY_EMAIL
import static net.ignaszak.manager.commons.test.helper.UserHelper.ANY_PUBLIC_ID
import static net.ignaszak.manager.users.TestConstants.VALID_EMAIL
import static net.ignaszak.manager.users.TestConstants.VALID_PASSWORD

@UtilityClass
final class TestHelper {

    public static final String ACTIVATION_CODE = "123456"
    public static final String RESET_PASSWORD_TOKEN = "bdcec1cf-3e11-4835-bf74-c737076060ba"

    static void expectUserResponse(ResultActions resultActions, ResultMatcher resultMatcher) {
        assertResponse(resultActions, resultMatcher, new DataResponse(anyUserResponse()))
    }

    static UserEntity anyUserEntity() {
        return new UserEntity(VALID_EMAIL, VALID_PASSWORD, LocalDateTime.now())
    }

    static UserRegistrationRequest anyUserRegistrationRequest() {
        return new UserRegistrationRequest(VALID_EMAIL, VALID_PASSWORD, VALID_PASSWORD)
    }

    static UserResetPasswordTokenRequest anyUserResetPasswordTokenRequest() {
        return new UserResetPasswordTokenRequest(VALID_EMAIL)
    }

    static UserActivationRequest anyUserActivationRequest() {
        return new UserActivationRequest(ACTIVATION_CODE)
    }

    static UserResetPasswordRequest anyUserResetPasswordRequest() {
        return new UserResetPasswordRequest(VALID_PASSWORD, VALID_PASSWORD)
    }

    static UserTO anyUserTO() {
        new UserTO(ANY_PUBLIC_ID, ANY_EMAIL, "")
    }

    static UserResponse anyUserResponse() {
        new UserResponse(ANY_PUBLIC_ID, ANY_EMAIL)
    }
}
