package net.ignaszak.manager.users.config;

import org.springframework.stereotype.Component;

import java.time.Period;

@Component
class AppConfigImpl implements AppConfig {

    private static final String CRON_EVERY_HOUR = "0 0 0/1 1/1 * ?";
    private static final Period DELETE_INACTIVE_USER_PERIOD = Period.ofDays(1);

    @Override
    public Period getDeleteInactiveUserPeriod() {
        return DELETE_INACTIVE_USER_PERIOD;
    }

    @Override
    public String getDeleteInactiveUserCronSchedule() {
        return CRON_EVERY_HOUR;
    }
}
