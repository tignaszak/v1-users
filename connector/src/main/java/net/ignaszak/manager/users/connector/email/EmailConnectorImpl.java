package net.ignaszak.manager.users.connector.email;

import lombok.AllArgsConstructor;
import net.ignaszak.manager.commons.email.data.ResetPasswordData;
import net.ignaszak.manager.commons.email.model.EmailModel;
import net.ignaszak.manager.commons.email.service.EmailServiceInterface;
import org.springframework.stereotype.Component;

import static net.ignaszak.manager.commons.email.pattern.EmailPattern.RESET_PASSWORD;

@Component
@AllArgsConstructor
class EmailConnectorImpl implements EmailConnectorInterface {

    private final EmailServiceInterface emailService;

    @Override
    public void sendResetPasswordEmail(String to, String token) {
        final var data = new ResetPasswordData(token);
        final var emailModel = new EmailModel(to, RESET_PASSWORD, data);
        emailService.send(emailModel);
    }
}
