package net.ignaszak.manager.users.security.model;

public record UserPrincipalModel(String publicId, String email, boolean isActive) {}
