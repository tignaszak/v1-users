package net.ignaszak.manager.users.to;

public record UserResetPasswordTO(String token, String password, String passwordRepeated) {}
