package net.ignaszak.manager.users.service.error;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import net.ignaszak.manager.commons.error.Error;

@Getter
@AllArgsConstructor
public enum UserError implements Error {
    USR_USER_EXISTS("USR-1", "User already exists!"),
    USR_INVALID_PASSWORD("USR-2", "Invalid password!"),
    USR_INVALID_EMAIL("USR-3", "Invalid email!"),
    USR_USER_NOT_FOUND("USR-4", "User not found!"),
    USR_BAD_CREDENTIALS("USR-5", "Bad credentials!"),
    USR_INVALID_PUBLIC_ID("USR-6", "Invalid public id!"),
    USR_INACTIVE("USR-7", "Inactive user!"),
    USR_INVALID_ACTIVATION_CODE("USR-8", "Invalid activation code!"),
    USR_ACTIVATION("USR-9", "Could not activate user!"),
    USR_INVALID_RESET_PASSWORD_TOKEN("USR-10", "Invalid reset password token!"),
    USR_ACTIVE("USR-11", "User already active!");

    String code;
    String message;

    @Override
    @NonNull
    public String getName() {
        return name();
    }

    @Override
    public String toString() {
        return getString();
    }
}
