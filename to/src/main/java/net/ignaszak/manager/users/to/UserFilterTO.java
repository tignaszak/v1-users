package net.ignaszak.manager.users.to;

import net.ignaszak.manager.commons.restcriteria.filtering.to.StringOperatorTO;

public record UserFilterTO(StringOperatorTO publicId, StringOperatorTO email) {}
