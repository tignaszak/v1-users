package net.ignaszak.manager.users.service.validator.impl;

import lombok.AllArgsConstructor;
import net.ignaszak.manager.commons.error.ErrorCollector;
import net.ignaszak.manager.commons.error.ErrorCollectorImpl;
import net.ignaszak.manager.commons.error.exception.ValidationException;
import net.ignaszak.manager.users.repository.user.UserRepository;
import net.ignaszak.manager.users.service.validator.UserValidator;
import net.ignaszak.manager.users.to.UserRegistrationTO;
import net.ignaszak.manager.users.to.UserResetPasswordTO;
import net.ignaszak.manager.users.to.UserUpdateTO;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.UUID;
import java.util.regex.Pattern;

import static net.ignaszak.manager.commons.error.Error.Default.UNKNOWN;
import static net.ignaszak.manager.commons.utils.SecurityUtils.isNotEmail;
import static net.ignaszak.manager.commons.utils.SecurityUtils.isNotPassword;
import static net.ignaszak.manager.users.service.error.UserError.*;
import static org.apache.commons.lang3.StringUtils.isBlank;

@Component
@AllArgsConstructor
class UserValidatorImpl implements UserValidator {

    private static final Pattern ACTIVATION_CODE_PATTERN = Pattern.compile("^\\d{6}$");

    private final UserRepository userRepository;

    private final ErrorCollector errorCollector = new ErrorCollectorImpl();

    @Override
    public void validRegistration(final UserRegistrationTO userRegistrationTO) {
        if (userRegistrationTO == null) {
            errorCollector.add(UNKNOWN);
        } else {
            validRegistrationEmail(userRegistrationTO.email());
            validPassword(userRegistrationTO.password(), userRegistrationTO.passwordRepeated());
        }
        errorCollector.throwException();
    }

    @Override
    public void validEmail(final String email) {
        if (isNotEmail(email)) {
            throw new ValidationException(USR_INVALID_EMAIL);
        }
    }

    @Override
    public void validPublicId(String publicId) {
        validPublicIdInner(publicId);

        errorCollector.throwException();
    }

    @Override
    public void validActivationCode(String activationCode) {
        if (isBlank(activationCode) || !ACTIVATION_CODE_PATTERN.matcher(activationCode).find()) {
            throw new ValidationException(USR_INVALID_ACTIVATION_CODE);
        }
    }

    @Override
    public void validResetPassword(UserResetPasswordTO userResetPasswordTO) {
        if (userResetPasswordTO == null) {
            errorCollector.add(UNKNOWN);
        } else {
            validToken(userResetPasswordTO.token());
            validPassword(userResetPasswordTO.password(), userResetPasswordTO.passwordRepeated());
        }
        errorCollector.throwException();
    }

    @Override
    public void validUpdate(final UserUpdateTO userUpdateTO) {
        validPublicIdInner(userUpdateTO.publicId());
        if (isNotPassword(userUpdateTO.oldPassword())) errorCollector.add(USR_INVALID_PASSWORD);
        validPassword(userUpdateTO.newPassword(), userUpdateTO.newPasswordRetyped());

        errorCollector.throwException();
    }

    private void validRegistrationEmail(final String email) {
        if (isNotEmail(email)) {
            errorCollector.add(USR_INVALID_EMAIL);
            return;
        }
        userRepository.findUserEntityByEmail(email)
            .ifPresent(userEntity -> errorCollector.add(USR_USER_EXISTS));
    }

    private void validPassword(final String password, final String passwordRetyped) {
        if (isNotPassword(password) || !Objects.equals(password, passwordRetyped)) {
            errorCollector.add(USR_INVALID_PASSWORD);
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void validToken(String token) {
        try {
            UUID.fromString(token);
        } catch (IllegalArgumentException exception){
            throw new ValidationException(USR_INVALID_RESET_PASSWORD_TOKEN);
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void validPublicIdInner(String publicId) {
        try {
            UUID.fromString(publicId == null ? "" : publicId);
        } catch (IllegalArgumentException exception) {
            errorCollector.add(USR_INVALID_PUBLIC_ID);
        }
    }
}
