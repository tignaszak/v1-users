package net.ignaszak.manager.users.security.service.impl;

import net.ignaszak.manager.users.entity.UserEntity;
import net.ignaszak.manager.users.repository.user.UserRepository;
import net.ignaszak.manager.users.security.model.UserPrincipalModel;
import net.ignaszak.manager.users.security.model.UserSecurityModel;
import net.ignaszak.manager.users.security.service.UserSecurityService;
import net.ignaszak.manager.users.service.error.exception.UserNotFoundException;
import net.ignaszak.manager.users.service.validator.UserValidator;
import org.springframework.stereotype.Service;

@Service
class UserSecurityServiceImpl implements UserSecurityService {

    private final UserRepository userRepository;
    private final UserValidator userValidator;

    UserSecurityServiceImpl(final UserRepository userRepository, final UserValidator userValidator) {
        this.userRepository = userRepository;
        this.userValidator = userValidator;
    }

    @Override
    public UserSecurityModel getUserSecurityModelByEmail(final String email) {
        userValidator.validEmail(email);

        return userRepository.findUserEntityByEmail(email)
            .map(this::toUserSecurityModel)
            .orElseThrow(UserNotFoundException::new);
    }

    private UserSecurityModel toUserSecurityModel(final UserEntity userEntity) {
        final var userPrincipalModel = new UserPrincipalModel(userEntity.getPublicId(), userEntity.getEmail(), userEntity.isActive());
        return new UserSecurityModel(userEntity.getPassword(), userPrincipalModel);
    }
}
