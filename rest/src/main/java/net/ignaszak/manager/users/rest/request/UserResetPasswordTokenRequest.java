package net.ignaszak.manager.users.rest.request;

import io.swagger.v3.oas.annotations.media.Schema;

import static net.ignaszak.manager.users.rest.openapi.ApiConstants.EMAIL_ADDRESS;

@Schema(title = "User reset password token request")
public record UserResetPasswordTokenRequest(@Schema(description = EMAIL_ADDRESS, required = true) String email) {}
