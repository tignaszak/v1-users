package net.ignaszak.manager.users.rest.request;

import io.swagger.v3.oas.annotations.media.Schema;

import static net.ignaszak.manager.users.rest.openapi.ApiConstants.USER_ACTIVATION_CODE;
import static net.ignaszak.manager.users.rest.openapi.ApiConstants.USER_ACTIVATION_CODE_EXAMPLE;

@Schema(title = "User activation request")
public record UserActivationRequest(
        @Schema(description = USER_ACTIVATION_CODE, example = USER_ACTIVATION_CODE_EXAMPLE, required = true) String code
) {}
