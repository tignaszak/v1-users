package net.ignaszak.manager.users.service.validator.impl

import net.ignaszak.manager.commons.error.exception.ValidationException
import net.ignaszak.manager.users.repository.user.UserRepository
import net.ignaszak.manager.users.service.error.UserError
import net.ignaszak.manager.users.service.validator.UserValidator
import net.ignaszak.manager.users.to.UserResetPasswordTO
import net.ignaszak.manager.users.to.UserUpdateTO
import spock.lang.Specification

import static net.ignaszak.manager.users.TestHelper.ACTIVATION_CODE
import static net.ignaszak.manager.users.TestHelper.ANY_PUBLIC_ID
import static net.ignaszak.manager.users.TestHelper.INVALID_EMAIL
import static net.ignaszak.manager.users.TestHelper.INVALID_NO_DIGIT_PASSWORD
import static net.ignaszak.manager.users.TestHelper.INVALID_NO_LETTER_PASSWORD
import static net.ignaszak.manager.users.TestHelper.INVALID_SHORT_PASSWORD
import static net.ignaszak.manager.users.TestHelper.NEW_VALID_PASSWORD
import static net.ignaszak.manager.users.TestHelper.RESET_PASSWORD_TOKEN
import static net.ignaszak.manager.users.TestHelper.VALID_EMAIL
import static net.ignaszak.manager.users.TestHelper.VALID_PASSWORD
import static net.ignaszak.manager.users.TestHelper.anyUserEntity
import static net.ignaszak.manager.users.TestHelper.anyUserRegistrationTO
import static net.ignaszak.manager.users.TestHelper.getUserRegistrationTO

class UserValidatorImplSpec extends Specification {

    private UserRepository userRepositoryMock = Mock()

    private UserValidator subject

    def setup() {
        subject = new UserValidatorImpl(userRepositoryMock)
    }

    def "no exception is thrown when user registration data are correct and email is not used yet"() {
        given:
        1 * userRepositoryMock.findUserEntityByEmail(VALID_EMAIL) >> Optional.empty()
        def model = anyUserRegistrationTO()

        when:
        subject.validRegistration(model)

        then:
        noExceptionThrown()
    }

    def "an exception is thrown when user registration data are correct and email is already used"() {
        given:
        1 * userRepositoryMock.findUserEntityByEmail(VALID_EMAIL) >> Optional.of(anyUserEntity())
        def model = anyUserRegistrationTO()

        when:
        subject.validRegistration(model)

        then:
        def e = thrown ValidationException
        e.getMessage() == UserError.USR_USER_EXISTS.toString()
    }

    def "an exception is thrown when email is incorrect and passwords are valid"() {
        given:
        def model = getUserRegistrationTO(INVALID_EMAIL, VALID_PASSWORD, VALID_PASSWORD)

        when:
        subject.validRegistration(model)

        then:
        def e = thrown ValidationException
        e.getMessage() == UserError.USR_INVALID_EMAIL.toString()
        0 * userRepositoryMock.findUserEntityByEmail(any())
    }

    def "an exception is thrown when password are correct but are not equals"() {
        given:
        1 * userRepositoryMock.findUserEntityByEmail(VALID_EMAIL) >> Optional.empty()
        def model = getUserRegistrationTO(VALID_EMAIL, VALID_PASSWORD, "anotherValidPassword123")

        when:
        subject.validRegistration(model)

        then:
        def e = thrown ValidationException
        e.getMessage() == UserError.USR_INVALID_PASSWORD.toString()
    }

    def "an exception is thrown when password is shorter than 8 chars"() {
        given:
        1 * userRepositoryMock.findUserEntityByEmail(VALID_EMAIL) >> Optional.empty()
        def model = getUserRegistrationTO(VALID_EMAIL, INVALID_SHORT_PASSWORD, INVALID_SHORT_PASSWORD)

        when:
        subject.validRegistration(model)

        then:
        def e = thrown ValidationException
        e.getMessage() == UserError.USR_INVALID_PASSWORD.toString()
    }

    def "an exception is thrown when password is longer than 8 chars but does not contains any number"() {
        given:
        1 * userRepositoryMock.findUserEntityByEmail(VALID_EMAIL) >> Optional.empty()
        def model = getUserRegistrationTO(VALID_EMAIL, INVALID_NO_DIGIT_PASSWORD, INVALID_NO_DIGIT_PASSWORD)

        when:
        subject.validRegistration(model)

        then:
        def e = thrown ValidationException
        e.getMessage() == UserError.USR_INVALID_PASSWORD.toString()
    }

    def "an exception is thrown when password is longer than 8 chars but does not contains any letter"() {
        given:
        1 * userRepositoryMock.findUserEntityByEmail(VALID_EMAIL) >> Optional.empty()
        def model = getUserRegistrationTO(VALID_EMAIL, INVALID_NO_LETTER_PASSWORD, INVALID_NO_LETTER_PASSWORD)

        when:
        subject.validRegistration(model)

        then:
        def e = thrown ValidationException
        e.getMessage() == UserError.USR_INVALID_PASSWORD.toString()
    }

    def "an multi message exception is thrown when all data are incorrect"() {
        given:
        def model = getUserRegistrationTO(INVALID_EMAIL, INVALID_SHORT_PASSWORD, INVALID_SHORT_PASSWORD)

        when:
        subject.validRegistration(model)

        then:
        def e = thrown ValidationException
        e.getErrorSet().size() == 2
        e.getMessage().contains(UserError.USR_INVALID_EMAIL.toString())
        e.getMessage().contains(UserError.USR_INVALID_EMAIL.toString())
        0 * userRepositoryMock.findUserEntityByEmail(any())
    }

    def "no exception is thrown when email is correct"() {
        when:
        subject.validEmail(VALID_EMAIL)

        then:
        noExceptionThrown()
    }

    def "an exception is thrown when email is incorrect"(String email) {
        when:
        subject.validEmail(email)

        then:
        def e = thrown ValidationException
        e.getMessage() == UserError.USR_INVALID_EMAIL.toString()

        where:
        email           | _
        "invalid@email" | _
        "test"          | _
        "test.com"      | _
        "@test"         | _
        "test@"         | _
        ".test"         | _
    }

    def "no exception is thrown when public id is correct"() {
        when:
        subject.validPublicId(ANY_PUBLIC_ID)

        then:
        noExceptionThrown()
    }

    def "an exception is thrown when public id is incorrect"(String publicId) {
        when:
        subject.validPublicId(publicId)

        then:
        def e = thrown ValidationException
        e.getMessage() == UserError.USR_INVALID_PUBLIC_ID.toString()

        where:
        publicId            | _
        ""                  | _
        "invalid public id" | _
    }

    def "no exception is thrown when activation code is correct"() {
        when:
        subject.validActivationCode(ACTIVATION_CODE)

        then:
        noExceptionThrown()
    }

    def "an exception is thrown when activation code is incorrect"(String activationCode) {
        when:
        subject.validActivationCode(activationCode)

        then:
        def e = thrown ValidationException
        e.getMessage() == UserError.USR_INVALID_ACTIVATION_CODE.toString()

        where:
        activationCode | _
        ""             | _
        "12345t"       | _
        "12"           | _
        "1234567"      | _
    }

    def "no exception is thrown when UserResetPasswordTO is correct" () {
        given:
        def to = new UserResetPasswordTO(RESET_PASSWORD_TOKEN, VALID_PASSWORD, VALID_PASSWORD)

        when:
        subject.validResetPassword(to)

        then:
        noExceptionThrown()
    }

    def "an exception is thrown when UserResetPasswordTO is incorrect" (String token, String password, String passwordRetyped) {
        given:
        def to = new UserResetPasswordTO(token, password, passwordRetyped)

        when:
        subject.validResetPassword(to)

        then:
        thrown(RuntimeException)

        where:
        token                | password       | passwordRetyped | _
        ""                   | ""             | ""              | _
        null                 | null           | null            | _
        ""                   | VALID_PASSWORD | VALID_PASSWORD  | _
        RESET_PASSWORD_TOKEN | ""             | VALID_PASSWORD  | _
    }

    def "no exception is thrown when UserUpdateTo is correct" () {
        given:
        def to = new UserUpdateTO(ANY_PUBLIC_ID, VALID_PASSWORD, NEW_VALID_PASSWORD, NEW_VALID_PASSWORD)

        when:
        subject.validUpdate(to)

        then:
        noExceptionThrown()
    }

    def "en exception is thrown when UserUpdateTO is incorrect" (String publicId, String oldPassword, String newPassword, String newPasswordRetyped) {
        given:
        def to = new UserUpdateTO(publicId, oldPassword, newPassword, newPasswordRetyped)

        when:
        subject.validUpdate(to)

        then:
        thrown ValidationException

        where:
        publicId      | oldPassword    | newPassword        | newPasswordRetyped | _
        null          | null           | null               | null               | _
        ""            | ""             | ""                 | ""                 | _
        ANY_PUBLIC_ID | "incorrect"    | NEW_VALID_PASSWORD | NEW_VALID_PASSWORD | _
        ANY_PUBLIC_ID | VALID_PASSWORD | NEW_VALID_PASSWORD | "incorrect"        | _
    }
}
